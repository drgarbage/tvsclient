//
//  MasterTableViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/30.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "MasterTableViewController.h"
#import "ConnectionEditorTableViewController.h"
#import "AppModel.h"

static MasterTableViewController *master;

@interface MasterTableViewController ()
@end

@implementation MasterTableViewController

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    master = self;
    
    if(AppModel.sharedModel.CurrentDevice == nil)
    [self performSegueWithIdentifier:@"showConnectionList" sender:self];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)dismissedFromViewController:(UIViewController *)viewController{
    //[self performSegueWithIdentifier:@"showLive" sender:self];
}

+ (void)showLive{
    if(master == nil) return;
    //[master performSegueWithIdentifier:@"showLive" sender:self];
}

@end
