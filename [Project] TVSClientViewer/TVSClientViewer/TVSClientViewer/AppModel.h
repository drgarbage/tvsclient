//
//  AppModel.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"

@interface AppModel : NSObject<NSCoding>
@property (nonatomic, retain, readonly) NSMutableArray * StoredDevices;
@property (nonatomic, retain) Device * CurrentDevice;
+ (AppModel*) sharedModel;
+ (Device*) newDevice;
+ (Device*) newDeviceWithURL: (NSString*) url user: (NSString*) user password: (NSString*) password;
+ (void) downloadFromURL: (NSString*) urlString
                    user: (NSString*) user
                password: (NSString*) pwd
                 success: (void(^)(NSString *responseText)) success
                    fail: (void(^)(NSError *error)) fail;
+ (void) downloadInfoFromDevice: (Device*) device
                        success: (void(^)(NSDictionary* result)) success
                           fail: (void(^)(NSError *error)) fail;
+ (void) downloadLogFromDevice: (Device*) device
                       success: (void(^)(NSDictionary* result)) success
                          fail: (void(^)(NSError *error)) fail;
+ (NSString*) DocumentPath: (NSString*) filename;
- (void) save;
@end

static AppModel * INSTANCE;
