//
//  TimeSelectorViewController.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/18.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TimeSelectorViewControllerDelegate <NSObject>
-(void)didUpdateTime;
@end

@interface TimeSelectorViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (nonatomic, retain) id<TimeSelectorViewControllerDelegate> delegate;
@end
