//
//  InfoTableViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/30.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "InfoTableViewController.h"
#import "AppModel.h"
#import "XMLDictionary.h"

@interface InfoTableViewController ()
@property (weak, nonatomic) IBOutlet UILabel *ModelName;
@property (weak, nonatomic) IBOutlet UILabel *FWVersion;
@property (weak, nonatomic) IBOutlet UILabel *HDCapacity;
@property (weak, nonatomic) IBOutlet UILabel *Channels;
@property (weak, nonatomic) IBOutlet UILabel *MacAddress;
@property (weak, nonatomic) IBOutlet UILabel *LocalDisplay;

@end

@implementation InfoTableViewController

//- (void) downloadFromURL: (NSString*) urlString
//                    user: (NSString*) user
//                password: (NSString*) pwd
//                 success: (void(^)(NSString *responseText)) success {
//    
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSString *basicAuthCredentials = [NSString stringWithFormat:@"%@:%@", user, pwd];
//    NSData *authData = [basicAuthCredentials dataUsingEncoding:NSASCIIStringEncoding];
//    NSString *base64EncodedAuth = [authData base64Encoding];
//    NSString *authValue = [NSString stringWithFormat:@"Basic %@", base64EncodedAuth];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
//    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//    [request setValue:@"close" forHTTPHeaderField:@"Connection"];
//    
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:[NSOperationQueue mainQueue]
//                           completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError){
//                               if(((NSHTTPURLResponse*)response).statusCode == 200) {
//                                   NSString *responseText = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
//                                   success(responseText);
//                               }
//                           }];
//}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    Device *device = [AppModel sharedModel].CurrentDevice;
    
    [AppModel downloadInfoFromDevice:device success:^(NSDictionary *dic) {
        _ModelName.text = dic[@"MODELNAME"];
        _FWVersion.text = dic[@"FWVERSION"];
        _HDCapacity.text = dic[@"HDDCAPACITY"];
        _Channels.text = dic[@"channels"];
        _MacAddress.text = dic[@"MACADDRESS"];
        _LocalDisplay.text = dic[@"localDisplay"];
    } fail: nil];
}

@end
