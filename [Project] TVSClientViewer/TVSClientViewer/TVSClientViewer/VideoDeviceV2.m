//
//  VideoDeviceV2.m
//  TVSClientViewer
//
//  Created by Johnson on 2016/1/20.
//  Copyright © 2016年 tvs. All rights reserved.
//

#import "VideoDeviceV2.h"
#import "StreamThread.h"
#define DONE 1
#define NOTDONE 0
#define LIVE_STREAM_PATTERN @"rtsp://%@/ch%03ld2.sdp"
#define PLAYBACK_STREAM_PATTERN @"rtsp://%@/ch%03ld2_%@.dsk"
int playspeeds[] = {-32, -16, -8, -4, -2, -1, 1, 2, 4, 8, 16, 32};

@interface VideoDeviceV2 (){
    NSArray *_channels;
}
@end
@implementation VideoDeviceV2
- (void)initialize{
    _channels = [NSArray arrayWithObjects:
                 [ChannelV2 channelOfDevice:self withIndex:1],
                 [ChannelV2 channelOfDevice:self withIndex:2],
                 [ChannelV2 channelOfDevice:self withIndex:3],
                 [ChannelV2 channelOfDevice:self withIndex:4],
                 [ChannelV2 channelOfDevice:self withIndex:5],
                 [ChannelV2 channelOfDevice:self withIndex:6],
                 [ChannelV2 channelOfDevice:self withIndex:7],
                 [ChannelV2 channelOfDevice:self withIndex:8],
                 nil];
}
- (instancetype)init{
    self = [super init];
    if(self){
        [self initialize];
    }
    return self;
}
- (instancetype)initWithURL:(NSString *)url
             user:(NSString *)userName
         password:(NSString *)password{
    self = [super initWithURL:url user:userName password:password];
    if(self){
        [self initialize];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initialize];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
}
-(NSArray*)channels{
    return _channels;
}
@end

@interface ChannelV2(){
    StreamThread *thread;
    int speedIndex;
}
@end
@implementation ChannelV2
+(id)channelOfDevice: (Device*) device withIndex: (NSInteger) index{
    Channel *channel = [ChannelV2 new];
    channel.device = device;
    channel.index = index;
    return channel;
}
-(BOOL)isFailConnection{
    return (thread == nil)?NO:thread.isFailConnection;
}
-(void)play:(NSDate*)stamp{
    NSString *url = (stamp == nil)?
        [NSString stringWithFormat:LIVE_STREAM_PATTERN, self.device.URL, (long)self.index]:
        [NSString stringWithFormat:PLAYBACK_STREAM_PATTERN, self.device.URL, (long)self.index, [self formatTimeParam: stamp]];
    
    thread = [[StreamThread alloc] init];
    [thread startStream:url stamp:stamp];
}
-(void)forward{
    if(thread == nil) return;
    speedIndex++;
    if(speedIndex >= sizeof playspeeds)
        speedIndex = (sizeof playspeeds) - 1;
    if(speedIndex < 6)
        speedIndex = 6;
    self.Speed = playspeeds[speedIndex];
    
    [thread playStream:(int)self.Speed];
}
-(void)backward{
    if(thread == nil) return;
    speedIndex--;
    if(speedIndex < 0)
        speedIndex = 0;
    if(speedIndex > 5)
        speedIndex = 5;
    self.Speed = playspeeds[speedIndex];
    
    [thread playStream:(int)self.Speed];
}
-(void)pause{
    if(thread == nil) return;
    [thread pauseStream];
}
-(void)stop{
    [thread dropStream];
    thread = nil;
}
-(void)mute{/* do nothing */}
-(void)unmute{/* do nothing */}
-(void)freeMemory{/* do nothing */}
-(NSString*)formatTimeParam:(NSDate*)stamp{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyyMMdd_HHmm00"];
    dateString = [formatter stringFromDate:stamp];
    return dateString;
}
-(UIImage *)frame{
    return thread.frame;
}
-(BOOL)isFrameReady{
    return thread.isFrameReady;
}
@end