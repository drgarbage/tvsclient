//
//  RTSPVideoServer.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/13.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "avformat.h"
#import "avcodec.h"
#import "avio.h"
#import "swscale.h"
#import "Queue.h"

#import "RTSPVideoServer.h"

#define DONE 1
#define NOTDONE 0
#define LIVE_STREAM_PATTERN @"rtsp://%@/ch%03ld2.sdp" //@"rtsp://%@/ch%03ld2_20151027_130000.dsk" //
#define PLAYBACK_STREAM_PATTERN @"rtsp://%@/ch%03ld2_%@.sdp" // ch0012_yyyyMMdd_HHmmss.dsk


@interface RTSPVideoServer (){
    AVFormatContext *pFormatCtx;
    AVCodecContext *pCodecCtx;
    AVFrame *pFrame;
    //AVPacket packet;
    AVPicture picture;
    int videoStream;
    int audioStream;
    struct SwsContext *img_convert_ctx;
    
    Queue *videoQueue;
    Queue *audioQueue;

    int outputWidth, outputHeight;
    
    BOOL isEnabled;
    BOOL isPrepared;
    
    NSThread *streamThread;
    NSConditionLock *streamThreadLock;
    BOOL isRunningStreamThread;
    BOOL isPaused;
    
    NSThread *decodeThread;
    NSConditionLock *decodeThreadLock;
    BOOL isRunningDecodeThread;
    
    NSDate *playbackStamp;
}
@end

@implementation RTSPVideoServer

-(id)init{
    self = [super init];
    if(self){
        videoQueue = [[Queue alloc] init];
        audioQueue = [[Queue alloc] init];
    }
    return self;
}

-(void)play:(NSDate*)stamp{
    playbackStamp = stamp;
    
    // check is switching between live or playback
    // 0: stop to live
    // 1: stop to playback
    // 2: live to playback
    // 3: playback to live
    
    // if thread not running
    // if thread running
    
    if(!isRunningStreamThread) {
        [self doStartThread];
    }else if(isRunningStreamThread && isPaused) {
        isPaused = FALSE;
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            av_read_play(pFormatCtx);
        });
    } else {
        // click play while already playing
    }
    
}
-(void)pause{
    if(!isRunningStreamThread || isPaused) return;
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        av_read_pause(pFormatCtx);
        isPaused = TRUE;
    });
}
-(void)stop{
    if(!isRunningStreamThread) return;
}
-(void)mute{
    if(!isRunningStreamThread) return;
}
-(void)unmute{
    if(!isRunningStreamThread) return;
}

///////////////////////////////////
-(void)doStartThread{
    
    if(!isPrepared){
        NSString *url = (playbackStamp == 0)?
            [NSString stringWithFormat:LIVE_STREAM_PATTERN, self.host, (long)self.channel]:
            [NSString stringWithFormat:PLAYBACK_STREAM_PATTERN, self.host, (long)self.channel, [self formatTimeParam: playbackStamp]];
        //url = @"rtsp://192.168.2.200/ch0012.sdp";
        //url = @"rtsp://192.168.1.1/MJPG?W=640&H=360&Q=50&BR=3000000";
        isPrepared = [self doPrepare: url];
    }
    
    if(!isPrepared) return;

    
    if (streamThread == nil) {
        streamThreadLock = [[NSConditionLock alloc] initWithCondition:NOTDONE];
        isRunningStreamThread = TRUE;
        streamThread = [[NSThread alloc] initWithTarget:self selector:@selector(doReceive:) object:self];
        streamThread.threadPriority = 2;
        [streamThread start];
    }
    
    if (decodeThread == nil) {
        decodeThreadLock = [[NSConditionLock alloc] initWithCondition:NOTDONE];
        isRunningDecodeThread = TRUE;
        decodeThread = [[NSThread alloc] initWithTarget:self selector:@selector(doDecode:) object:self];
        streamThread.threadPriority = 5;
        [decodeThread start];
    }
}
-(void)doStopThread{
    isRunningStreamThread = FALSE;
    isRunningDecodeThread = FALSE;
    
    if (streamThread != nil) {
        [streamThreadLock lockWhenCondition:DONE];
        [streamThreadLock unlock];
        streamThreadLock = nil;
        
        [streamThread cancel];
        streamThread = nil;
    }
    
    if (decodeThread != nil){
        [decodeThreadLock lockWhenCondition:DONE];
        [decodeThreadLock unlock];
        decodeThreadLock = nil;
        
        [decodeThread cancel];
        decodeThread = nil;
    }
    
}
-(BOOL)doPrepare: (NSString*) url {
    AVCodec *pCodec;
    
    // Register all formats and codecs
    avcodec_register_all();
    av_register_all();
    avformat_network_init();
    
    // Set the RTSP Options
    AVDictionary *opts = 0;
    av_dict_set(&opts, "rtsp_transport", "tcp", 0);
    
    if (avformat_open_input(&pFormatCtx, [url UTF8String], NULL, &opts) !=0 ) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't open file\n");
        goto initError;
    }
    
    // Retrieve stream information
    if (avformat_find_stream_info(pFormatCtx,NULL) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't find stream information\n");
        goto initError;
    }
    
    // Find the first video stream
    videoStream=-1; audioStream=-1;
    for (int i=0; i<pFormatCtx->nb_streams; i++) {
        if (pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) { videoStream=i; }
        if (pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO) { audioStream=i; }
    }
    if (videoStream==-1 && audioStream==-1) { goto initError; }
    
    // Get a pointer to the codec context for the video stream
    pCodecCtx = pFormatCtx->streams[videoStream]->codec;
    //pCodecCtx->get_format        = AV_PIX_FMT_YUV420P;
    //pCodecCtx->idct_algo = FF_IDCT_AUTO;
    //pCodecCtx->flags2          |= CODEC_FLAG2_FAST | CODEC_FLAG2_CHUNKS;
    //pCodecCtx->skip_frame        = AVDISCARD_NONE;//AVDISCARD_DEFAULT;
    //pCodecCtx->skip_idct         = AVDISCARD_DEFAULT;
    //pCodecCtx->skip_loop_filter  = AVDISCARD_DEFAULT;
    //pCodecCtx->error_concealment = 256;
    
    // Find the decoder for the video stream
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if (pCodec == NULL) { goto initError; }
    
    // Open codec
    if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) { goto initError; }
    
    //if (audioStream > -1 ) { [self setupAudioDecoder]; }
    
    // Allocate video frame
    pFrame = av_frame_alloc(); //avcodec_alloc_frame();
    
    outputWidth = pCodecCtx->width;
    outputHeight = pCodecCtx->height;
    
    NSLog(@"pCodecCtx SIZE: %i %i", pCodecCtx->width, pCodecCtx->height);
    
    [self setupScaler];
    
    return YES;
    
initError:
    return NO;
    
}
-(void)doReceive:(id)state {
    [streamThreadLock lock];
    
    while (isPrepared && isRunningStreamThread) {
        
        AVPacket packet;

        if(av_read_frame(pFormatCtx, &packet)<0) continue;
        
        if(packet.stream_index == videoStream) {
            NSData * data = [[NSData alloc] initWithBytesNoCopy:packet.data length:packet.size];
//            NSData * data = [NSData dataWithBytes:packet.data
//                                            length:packet.size];
            [videoQueue enqueue:data];
        }
        
        if(packet.stream_index == audioStream) {
            
        }
        
        
        //av_free_packet(&packet);
        
        [NSThread sleepForTimeInterval:1.0/50.0];
    }
    [streamThreadLock unlockWithCondition:DONE];
}
-(void)doDecode:(id)state{
    NSTimeInterval interval = 1.0/35.0;

    [decodeThreadLock lock];
    
    while(isRunningStreamThread){
        
        @autoreleasepool {
            
            NSData* pktData;
//            if([videoQueue count] > 60) {
//                int frameType = FRAME_TYPE_UNKNOW;
//                while(frameType != FRAME_TYPE_I && [videoQueue count] > 0) {
//                    pktData = [videoQueue dequeue];
//                    frameType = CheckH264IorP((void*)pktData.bytes, pktData.length);
//                }
//            } else {
//                pktData = [videoQueue dequeue];
//            }
            pktData = [videoQueue dequeue];
            
            if (pktData == nil) {
                [NSThread sleepForTimeInterval:interval];
                continue;
            }
            
            AVPacket pkt;
            pkt.data = (void*)pktData.bytes;
            pkt.size = (int)pktData.length;
            pktData = nil;

            av_init_packet(&pkt);
            
            int got_gop = 0;
            
            avcodec_decode_video2(pCodecCtx, pFrame, &got_gop, &pkt);
            av_free_packet(&pkt);

            if (got_gop <= 0 || !pFrame->data[0]) {
                [NSThread sleepForTimeInterval:interval];
                continue;
            }
            
            sws_scale(img_convert_ctx, (const uint8_t *const *)pFrame->data, pFrame->linesize,
                      0, pCodecCtx->height, picture.data, picture.linesize);
            
            CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
            CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, picture.data[0], picture.linesize[0]*outputHeight,kCFAllocatorNull);
            CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            CGImageRef cgImage = CGImageCreate(outputWidth,
                                               outputHeight,
                                               8,
                                               24,
                                               picture.linesize[0],
                                               colorSpace,
                                               bitmapInfo,
                                               provider,
                                               NULL,
                                               NO,
                                               kCGRenderingIntentDefault);
            CGColorSpaceRelease(colorSpace);
            _frame = [UIImage imageWithCGImage:cgImage];
            
            CGImageRelease(cgImage);
            CGDataProviderRelease(provider);
            CFRelease(data);
            
            [NSThread sleepForTimeInterval:interval];
            
        }
    }
    
    [decodeThreadLock unlockWithCondition:DONE];
}
-(void)setupAudioDecoder {
    /*
    if (audioStream >= 0) {
        _audioBufferSize = AVCODEC_MAX_AUDIO_FRAME_SIZE;
        _audioBuffer = av_malloc(_audioBufferSize);
        _inBuffer = NO;
        
        _audioCodecContext = pFormatCtx->streams[audioStream]->codec;
        _audioStream = pFormatCtx->streams[audioStream];
        
        AVCodec *codec = avcodec_find_decoder(_audioCodecContext->codec_id);
        if (codec == NULL) {
            NSLog(@"Not found audio codec.");
            return;
        }
        
        if (avcodec_open2(_audioCodecContext, codec, NULL) < 0) {
            NSLog(@"Could not open audio codec.");
            return;
        }
        
        if (audioPacketQueue) {
            [audioPacketQueue release];
            audioPacketQueue = nil;
        }
        audioPacketQueue = [[NSMutableArray alloc] init];
        
        if (audioPacketQueueLock) {
            [audioPacketQueueLock release];
            audioPacketQueueLock = nil;
        }
        audioPacketQueueLock = [[NSLock alloc] init];
        
    } else {
        pFormatCtx->streams[audioStream]->discard = AVDISCARD_ALL;
        audioStream = -1;
    }*/
}
-(void)setupScaler {
    // Release old picture and scaler
    avpicture_free(&picture);
    sws_freeContext(img_convert_ctx);
    
    // Allocate RGB picture
    avpicture_alloc(&picture, PIX_FMT_RGB24, outputWidth, outputHeight);
    
    // Setup scaler
    static int sws_flags =  SWS_FAST_BILINEAR;
    img_convert_ctx = sws_getContext(pCodecCtx->width,
                                     pCodecCtx->height,
                                     pCodecCtx->pix_fmt,
                                     outputWidth,
                                     outputHeight,
                                     PIX_FMT_RGB24,
                                     sws_flags, NULL, NULL, NULL);
    
}


///////////////////////////////////
-(UIImage *)imageFromAVPicture:(AVPicture)pict {
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, pict.data[0], pict.linesize[0]*outputHeight,kCFAllocatorNull);
    CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef cgImage = CGImageCreate(outputWidth,
                                       outputHeight,
                                       8,
                                       24,
                                       pict.linesize[0],
                                       colorSpace,
                                       bitmapInfo,
                                       provider,
                                       NULL,
                                       NO,
                                       kCGRenderingIntentDefault);
    CGColorSpaceRelease(colorSpace);
    UIImage *image = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    CGDataProviderRelease(provider);
    CFRelease(data);
    
    NSLog(@">>> Output Image: %f, %f", image.size.width, image.size.height);
    
    return image;
}
-(NSString*)formatTimeParam:(NSDate*)stamp{
    NSDateFormatter *formatter;
    NSString        *dateString;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyyMMdd_HHmmss"];
    dateString = [formatter stringFromDate:stamp];
    return dateString;
}
@end
