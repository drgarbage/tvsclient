//
//  TimeSelectorViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/18.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "TimeSelectorViewController.h"

@interface TimeSelectorViewController ()
-(IBAction)timeChanged:(id)sender;
@end

@implementation TimeSelectorViewController
-(void)timeChanged:(id)sender{
    if(!self.delegate) return;
    [self dismissViewControllerAnimated:YES completion:^(void){
        [self.delegate didUpdateTime];
    }];
}
@end
