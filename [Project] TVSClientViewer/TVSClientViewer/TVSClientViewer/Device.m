//
//  Device.m
//  TVSClientViewer
//
//  Created by Johnson on 2016/1/25.
//  Copyright © 2016年 tvs. All rights reserved.
//

#import "Device.h"

@implementation Device
-(instancetype)initWithURL:(NSString*) url
                      user:(NSString*) userName
                  password:(NSString*) password{
    self = [super init];
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        self.Name = [aDecoder decodeObjectForKey:@"Name"];
        self.URL = [aDecoder decodeObjectForKey:@"URL"];
        self.UserName = [aDecoder decodeObjectForKey:@"UserName"];
        self.Password = [aDecoder decodeObjectForKey:@"Password"];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.Name forKey:@"Name"];
    [aCoder encodeObject:self.URL forKey:@"URL"];
    [aCoder encodeObject:self.UserName forKey:@"UserName"];
    [aCoder encodeObject:self.Password forKey:@"Password"];
}
-(void)connect{}
-(void)disconnect{}
-(void)freeMemory{}
@end

@implementation Channel
//+(id)channelOfDevice: (Device*) device
//           withIndex: (NSInteger) index{
//    Channel *channel = [Channel new];
//    channel.device = device;
//    channel.index = index;
//    return channel;
//}
-(void)play:(NSDate*)stamp{/* do nothing */}
-(void)forward{/* do nothing */}
-(void)backward{/* do nothing */}
-(void)pause{/* do nothing */}
-(void)stop{/* do nothing */}
-(void)mute{/* do nothing */}
-(void)unmute{/* do nothing */}
-(void)freeMemory{/* do nothing */}
@end