//
//  Device.h
//  TVSClientViewer
//
//  Created by Johnson on 2016/1/25.
//  Copyright © 2016年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define STATUS_STOP        0x00
#define STATUS_PAUSED      0x01
#define STATUS_PLAYING     0x02

@interface Device : NSObject <NSCoding>
@property (nonatomic, retain) NSString *Name;
@property (nonatomic, retain) NSString *URL;
@property (nonatomic, retain) NSString *UserName;
@property (nonatomic, retain) NSString *Password;
@property (nonatomic, readonly) NSArray *channels;
-(id)initWithURL:(NSString*) url user:(NSString*) userName password: (NSString*) password;
-(void)connect;
-(void)disconnect;
-(void)freeMemory;
@end

@protocol ChannelDelegate <NSObject>
-(void)didUpdateFrame;
@end

@interface Channel : NSObject
@property (nonatomic, retain) id<ChannelDelegate> delegate;
@property (nonatomic, retain) Device *device;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, retain) UIImage *frame;
@property (nonatomic, assign) NSInteger Speed;
@property (nonatomic, assign, readonly) NSInteger status;
@property (nonatomic, retain, readonly) NSString *LiveInfo;
@property (nonatomic, assign, readonly) BOOL isContinueStreaming;
@property (nonatomic, assign, readonly) BOOL isFrameReady;
@property (nonatomic, assign, readonly) BOOL isFailConnection;
//+(id)channelOfDevice: (Device*) device withIndex: (NSInteger) index;
-(void)play:(NSDate*)stamp;
-(void)forward;
-(void)backward;
-(void)pause;
-(void)stop;
-(void)mute;
-(void)unmute;
-(void)freeMemory;
@end