//
//  VideoDevice.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/11.
//  Copyright (c) 2015年 tvs. All rights reserved.
//

#include <math.h>
#import "VideoDevice.h"
#import "RTSPPlayer.h"
#import "UPnPManager.h"
#import "Queue.h"
#import "avformat.h"
#import "SeiEncDec.h"

typedef uint32_t DWORD;
typedef uint8_t BYTE;

#define FRAME_TYPE_UNKNOW  0x00 /**< Type unknow */
#define FRAME_TYPE_I       0x01 /**< I Frame */
#define FRAME_TYPE_P       0X02 /**< P Frame */

static int CheckH264IorP (BYTE* pData, DWORD dwLen)
{
    
    unsigned long nSize = dwLen;
    if( nSize < 4 )
        return FRAME_TYPE_UNKNOW;
    
    while( (4 <= nSize) && ((0 != pData[0]) || (0 != pData[1]) || (1 != pData[2]) ) )
    {
        pData += 1;
        nSize -= 1;
    }
    
    if( 4 <= nSize )
    {
        int n = ((pData[0] << 24) | (pData[1] << 16) | (pData[2] << 8) | (pData[3])) & 0x1f;
        
        switch( n )
        {
            case 2://NAL_UT_DPA
            case 3://NAL_UT_DPB
            case 4://NAL_UT_DPC
            case 5://NAL_UT_IDR_SLICE:
            case 7:
            case 8:
                return FRAME_TYPE_I;
                break;
        }
    }
    return FRAME_TYPE_P;
}

int speeds[] = {-32, -16, -8, -4, -2, -1, 1, 2, 4, 8, 16, 32};

#define STREAM_PATTERN @"rtsp://%@/ch%03ld2.sdp" //@"rtsp://%@/ch%03ld2_20151027_130000.dsk" //
#define PLAYBACK_STREAM_PATTERN @"rtsp://%@/ch%03ld2_%@.dsk" // ch0012_yyyyMMdd_HHmmss.dsk

@implementation VideoDevice {
    NSArray * _channels;
}
-(void)initialize{
    _channels = [NSArray arrayWithObjects:
                 [VideoChannel channelOfDevice: self withIndex: 1],
                 [VideoChannel channelOfDevice: self withIndex: 2],
                 [VideoChannel channelOfDevice: self withIndex: 3],
                 [VideoChannel channelOfDevice: self withIndex: 4],
                 [VideoChannel channelOfDevice: self withIndex: 5],
                 [VideoChannel channelOfDevice: self withIndex: 6],
                 [VideoChannel channelOfDevice: self withIndex: 7],
                 [VideoChannel channelOfDevice: self withIndex: 8],
                 nil];
}
-(id)initWithURL:(NSString*) url
            user:(NSString*) userName
        password: (NSString*) password{
    self = [super initWithURL:url user:userName password:password];
    if(self){
        [self initialize];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initialize];
    }
    return self;
}
-(void)encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
}
//-(instancetype)initWithCoder:(NSCoder *)aDecoder{
//    self = [super init];
//    if(!self){
//        self.Name = [aDecoder decodeObjectForKey:@"Name"];
//        self.URL = [aDecoder decodeObjectForKey:@"URL"];
//        self.UserName = [aDecoder decodeObjectForKey:@"UserName"];
//        self.Password = [aDecoder decodeObjectForKey:@"Password"];
//        [self initialize];
//    }
//    return self;
//}
//-(void)encodeWithCoder:(NSCoder *)aCoder{
//    [aCoder encodeObject:self.Name forKey:@"Name"];
//    [aCoder encodeObject:self.URL forKey:@"URL"];
//    [aCoder encodeObject:self.UserName forKey:@"UserName"];
//    [aCoder encodeObject:self.Password forKey:@"Password"];
//}
-(NSArray*)channels{
    return _channels;
}
-(void)freeMemory{
    [super freeMemory];
    for(Channel* ch in _channels){
        [ch freeMemory];
    }
}
@end

#define DONE 1
#define NOTDONE 0
#define LIVE_STREAM_PATTERN @"rtsp://%@/ch%03ld2.sdp"
#define PLAYBACK_STREAM_PATTERN @"rtsp://%@/ch%03ld2_%@.dsk"

int vdo_decode_interrupt_cb(void *ctx){
    Channel *channel = (__bridge Channel*)ctx;
    return (channel.isContinueStreaming)?0:1;
};

@interface VideoChannel (){
    AVFormatContext *pFormatCtx;
    AVCodecContext *pVideoCodecCtx;
    AVCodecContext *pAudioCodecCtx;
    AVCodec *pVideoCodec;
    AVCodec *pAudioCodec;
    AVFrame *pFrame;
    //AVPacket packet;
    AVPicture picture;
    int videoStream;
    int audioStream;
    struct SwsContext *pSwsCtx;
    
    Queue *videoQueue;
    Queue *audioQueue;
    
    int outputWidth, outputHeight;
    
    BOOL isEnabled;
    BOOL isPrepared;
    
    NSThread *streamThread;
    NSConditionLock *streamThreadLock;
    BOOL isRunningStreamThread;
    BOOL isPaused;
    
    NSThread *decodeThread;
    NSConditionLock *decodeThreadLock;
    BOOL isRunningDecodeThread;
    
    NSDate *playbackStamp;
    int speedIndex;
    
    SUB_DEV_T gpsdata;
    
    int frameCount;
    
    
    BOOL
        is_avformat_open_input,
        is_avformat_find_stream_info,
        is_stream_id_found,
        is_avcodec_found,
        is_avcodec_video_open,
        is_avcodec_audio_open;
    
    float gps0, gps1, speed, gsenx, gseny, gsenz;
}
@end

@implementation VideoChannel

+(id)channelOfDevice: (Device*) device
           withIndex: (NSInteger) index{
    VideoChannel *channel = [VideoChannel new];
    channel.device = device;
    channel.index = index;
    return channel;
}
-(id)init{
    self = [super init];
    if(self){
        speedIndex = 6;
        videoQueue = [[Queue alloc] init];
        audioQueue = [[Queue alloc] init];
        
        av_register_all();
        avformat_network_init();
        pFormatCtx = NULL;
        pVideoCodecCtx = NULL;
        pFrame = NULL;
        pSwsCtx = NULL;
    }
    return self;
}
-(NSString *)LiveInfo{
    gps0 = (isnan(gpsdata.gps0))?gps0:gpsdata.gps0;
    gps1 = (isnan(gpsdata.gps1))?gps1:gpsdata.gps1;
    speed = (isnan(gpsdata.speed))?speed:gpsdata.speed;
    gsenx = (isnan(gpsdata.gsenx))?gsenx:gpsdata.gsenx;
    gseny = (isnan(gpsdata.gseny))?gseny:gpsdata.gseny;
    gsenz = (isnan(gpsdata.gsenz))?gps0:gpsdata.gsenz;
    return [NSString stringWithFormat:@"GPS: %@ %@ Speed: %@ KM G-Sen: x %@ y %@ z %@",
            (isnan(gps0))?[NSString stringWithFormat:@"%.2f",gps0]:@"---.------",
            (isnan(gps1))?[NSString stringWithFormat:@"%.2f",gps1]:@"---.------",
            (isnan(speed))?[NSString stringWithFormat:@"%.2f",speed]:@"-.--",
            (isnan(gsenx))?[NSString stringWithFormat:@"%.2f",gsenx]:@"-.--",
            (isnan(gseny))?[NSString stringWithFormat:@"%.2f",gseny]:@"-.--",
            (isnan(gsenz))?[NSString stringWithFormat:@"%.2f",gsenz]:@"-.--"
            ];
}
-(NSInteger)status{
    if(isPaused)
        return STATUS_PAUSED;
    if(isRunningStreamThread)
        return STATUS_PLAYING;
    return STATUS_STOP;
}
-(BOOL)isContinueStreaming{
    return isRunningStreamThread;
}
-(BOOL)isFrameReady{
    return frameCount > 30;
}
-(void)play:(NSDate*)stamp{
    frameCount = 0;
    playbackStamp = stamp;
    speedIndex = 6;
    self.Speed = speeds[speedIndex];
    
    if(!isRunningStreamThread) {
        [self doStartThread];
    }else if(isRunningStreamThread && isPaused && pFormatCtx != NULL) {
        isPaused = FALSE;
        dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @synchronized(self) {
                av_read_play(pFormatCtx);
            }
        });
    } else {
        // click play while already playing
    }
    
}
-(void)forward{
    if(!isRunningStreamThread || isPaused || pFormatCtx == NULL) return;
    
    speedIndex++;
    if(speedIndex >= sizeof speeds)
        speedIndex = (sizeof speeds) - 1;
    if(speedIndex < 6)
        speedIndex = 6;
    self.Speed = speeds[speedIndex];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            pFormatCtx->ts_id = (int)self.Speed;
            av_read_pause(pFormatCtx);
            av_read_play(pFormatCtx);
        }
    });
}
-(void)backward{
    if(!isRunningStreamThread || isPaused || pFormatCtx == NULL) return;
    
    speedIndex--;
    if(speedIndex < 0)
        speedIndex = 0;
    if(speedIndex > 5)
        speedIndex = 5;
    self.Speed = speeds[speedIndex];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            pFormatCtx->ts_id = (int)self.Speed;
            av_read_pause(pFormatCtx);
            av_read_play(pFormatCtx);
        }
    });
}
-(void)pause{
    if(!isRunningStreamThread || isPaused || pFormatCtx == NULL) return;
    speedIndex = 6;
    self.Speed = speeds[speedIndex];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            av_read_pause(pFormatCtx);
            isPaused = TRUE;
        }
    });
}
-(void)stop{
    if(!isRunningStreamThread) return;
    speedIndex = 6;
    self.Speed = speeds[speedIndex];
    
    [self doStopThread];
}
-(void)mute{
    if(!isRunningStreamThread) return;
}
-(void)unmute{
    if(!isRunningStreamThread) return;
}
-(void)freeMemory{
    [videoQueue clear];
    [audioQueue clear];
}
///////////////////////////////////
-(BOOL)doPrepare:(NSString*)url {
    is_avformat_open_input = NO;
    is_avformat_find_stream_info = NO;
    is_stream_id_found = NO;
    is_avcodec_found = NO;
    is_avcodec_video_open = NO;
    is_avcodec_audio_open = NO;
    
    // Set the RTSP Options
    AVDictionary *opts = 0;
    av_dict_set(&opts, "rtsp_transport", "tcp", 0);
    
    if (avformat_open_input(&pFormatCtx, [url UTF8String], NULL, &opts) !=0 ) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't open file\n");
        is_avformat_open_input = NO;
        goto Finish;
    } else {
        is_avformat_open_input = YES;
    }
    
    // Retrieve stream information
    if (avformat_find_stream_info(pFormatCtx,NULL) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't find stream information\n");
        is_avformat_find_stream_info = NO;
        goto Finish;
    } else {
        is_avformat_find_stream_info = YES;
    }
    
    // Find the first video stream
    videoStream=-1; audioStream=-1;
    for (int i=0; i<pFormatCtx->nb_streams; i++) {
        if (pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) { videoStream=i; }
        if (pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO) { audioStream=i; }
    }
    if (videoStream==-1 && audioStream==-1) {
        is_stream_id_found = NO;
        goto Finish;
    } else {
        is_stream_id_found = YES;
    }
    
    AVIOInterruptCB int_cb = { vdo_decode_interrupt_cb, (__bridge void *)(self) };
    pFormatCtx->interrupt_callback = int_cb;
    
    //
    // Setup Video Codec
    //
    pVideoCodecCtx = pFormatCtx->streams[videoStream]->codec;
    pVideoCodec = avcodec_find_decoder(pVideoCodecCtx->codec_id);
    if (pVideoCodec == NULL) { goto Finish; }
    if (avcodec_open2(pVideoCodecCtx, pVideoCodec, NULL) < 0) {
        pVideoCodecCtx = NULL;
        is_avcodec_video_open = NO;
        goto Finish;
    } else {
        is_avcodec_video_open = YES;
    }
    
    //
    // Setup Audio Codec
    //
    //    if (audioStream <= 0) {
    //        //pFormatCtx->streams[audioStream]->discard = AVDISCARD_ALL;
    //        audioStream = -1;
    //    } else {
    //        pAudioCodecCtx = pFormatCtx->streams[audioStream]->codec;
    //        pAudioCodec = avcodec_find_decoder(pAudioCodecCtx->codec_id);
    //        if (pAudioCodec != NULL) {
    //            if (avcodec_open2(pAudioCodecCtx, pAudioCodec, NULL) < 0) {
    //                pAudioCodecCtx = NULL;
    //            } else {
    //                is_avcodec_audio_open = NO;
    //            }
    //        } else {
    //            is_avcodec_audio_open = NO;
    //        }
    //    }
    
    outputWidth = pVideoCodecCtx->width;
    outputHeight = pVideoCodecCtx->height;
    
    [self setupScaler];
    
    return YES;
Finish:
    NSLog(@"CH %i PREPARE FAILED:", self.index);
    NSLog(@"CH %i is_avformat_open_input: %@", self.index, (is_avformat_open_input)?@"YES":@"NO");
    NSLog(@"CH %i is_avformat_find_stream_info: %@", self.index, (is_avformat_find_stream_info)?@"YES":@"NO");
    NSLog(@"CH %i is_stream_id_found: %@", self.index, (is_stream_id_found)?@"YES":@"NO");
    NSLog(@"CH %i is_avcodec_found: %@", self.index, (is_avcodec_found)?@"YES":@"NO");
    
    if(is_avcodec_found && pVideoCodecCtx != NULL) {
        avcodec_close(pVideoCodecCtx);
        is_avcodec_found = NO;
    }
    
    if(is_avcodec_audio_open && pAudioCodecCtx != NULL) {
        avcodec_close(pAudioCodecCtx);
        is_avcodec_audio_open = NO;
    }
    
    if(is_avformat_open_input && pFormatCtx!=NULL) {
        avformat_close_input(&pFormatCtx);
        is_avformat_open_input = NO;
    }
    
    if(pFrame != NULL){
        av_frame_free(&pFrame);
    }
    
    return NO;
}
-(void)doStartThread{

    @synchronized(self) {
        @autoreleasepool {
            
            NSString *url = (playbackStamp == nil)?
            [NSString stringWithFormat:LIVE_STREAM_PATTERN, self.device.URL, (long)self.index]:
            [NSString stringWithFormat:PLAYBACK_STREAM_PATTERN, self.device.URL, (long)self.index, [self formatTimeParam: playbackStamp]];
            NSLog(@"%i doStartThread %@", self.index, url);
        
            int retry = 0;
            
            while (!isPrepared) {
                isPrepared = [self doPrepare:url];
                retry++;
                if(retry > 10) return;
            }
        }
        
        if (streamThreadLock != nil) {
            NSLog(@"%i decodeThreadLock is not nil.", self.index);
        }
        
        if (streamThread == nil) {
            NSLog(@"%i doStartThread:[[NSConditionLock alloc] initWithCondition:NOTDONE]", self.index);
            streamThreadLock = [[NSConditionLock alloc] initWithCondition:NOTDONE];
            isRunningStreamThread = TRUE;
            streamThread = [[NSThread alloc] initWithTarget:self selector:@selector(doReceive:) object:nil];
            streamThread.threadPriority = 2;
            [streamThread start];
        }
        
        if (decodeThreadLock != nil) {
            NSLog(@"%i decodeThreadLock is not nil.", self.index);
        }
        
        if (decodeThread == nil) {
            decodeThreadLock = [[NSConditionLock alloc] initWithCondition:NOTDONE];
            isRunningDecodeThread = TRUE;
            decodeThread = [[NSThread alloc] initWithTarget:self selector:@selector(doDecode:) object:self];
            streamThread.threadPriority = 5;
            [decodeThread start];
        }
    }
}
-(void)doStopThread{
    @synchronized(self) {
        if (isRunningStreamThread) {
            isRunningStreamThread = FALSE;
            if(streamThread != nil){
                NSLog(@"%i doStopThread:[streamThreadLock lockWhenCondition:DONE]", self.index);
                [streamThreadLock lockWhenCondition:DONE];
                [streamThreadLock unlock];
                streamThreadLock = nil;
                
                [streamThread cancel];
                streamThread = nil;
            }
        }
        
        if (isRunningDecodeThread){
            isRunningDecodeThread = FALSE;
            if(decodeThread != nil){
                [decodeThreadLock lockWhenCondition:DONE];
                [decodeThreadLock unlock];
                decodeThreadLock = nil;
                
                [decodeThread cancel];
                decodeThread = nil;
            }
        }
        
        [self doRelease];
    }
    
}
-(void)doRelease{
    NSLog(@"CH %i Releasing:", self.index);
    NSLog(@"CH %i is_avformat_open_input: %@", self.index, (is_avformat_open_input)?@"YES":@"NO");
    NSLog(@"CH %i is_avformat_find_stream_info: %@", self.index, (is_avformat_find_stream_info)?@"YES":@"NO");
    NSLog(@"CH %i is_stream_id_found: %@", self.index, (is_stream_id_found)?@"YES":@"NO");
    NSLog(@"CH %i is_avcodec_found: %@", self.index, (is_avcodec_found)?@"YES":@"NO");
    
    if(is_avcodec_video_open && pVideoCodecCtx != NULL) {
        avcodec_close(pVideoCodecCtx);
        is_avcodec_video_open = NO;
    }
    
    if(is_avcodec_audio_open && pAudioCodecCtx != NULL) {
        avcodec_close(pAudioCodecCtx);
        is_avcodec_audio_open = NO;
    }
    
    if(is_avformat_open_input && pFormatCtx!=NULL) {
        avformat_close_input(&pFormatCtx);
        is_avformat_open_input = NO;
    }
    
    if(pFrame != NULL){
        av_frame_free(&pFrame);
    }
    
    [videoQueue clear];
    [audioQueue clear];
    
    isPrepared = NO;
}
-(void)doReceive:(id)state{
    NSLog(@"doReceive:[streamThreadLock lock]");
    [streamThreadLock lock];
    
    while (isRunningStreamThread) {
        
        if(isPaused) {
            [NSThread sleepForTimeInterval:1.0/60.0];
            continue;
        }
        
        AVPacket packet;
        
        @synchronized(self) {
            NSLog(@"av_read_frame");
            if(av_read_frame(pFormatCtx, &packet)<0)
                continue;
        }
        
        if(packet.stream_index == videoStream) {
            NSLog(@"videoStream");
            @autoreleasepool {
                NSData * data = [NSData dataWithBytes:packet.data length:packet.size];
                [videoQueue enqueue:data];
                av_free_packet(&packet);
            }
        }
        
        if(packet.stream_index == audioStream) {
            NSLog(@"audioStream");
            @autoreleasepool {
                NSData * data = [NSData dataWithBytes:packet.data length:packet.size];
                [audioQueue enqueue:data];
                av_free_packet(&packet);
            }
        }

        [NSThread sleepForTimeInterval:1.0/60.0];
//        NSLog(@"Packet...");
    }
    [streamThreadLock unlockWithCondition:DONE];
}
-(void)doDecode:(id)state{
    NSTimeInterval interval = 1.0/30.0;
    int packetLimit = 30;
    
    [decodeThreadLock lock];
    while(isRunningStreamThread){
        
        if(pVideoCodecCtx == NULL || pSwsCtx == NULL || videoQueue == nil) {
            [NSThread sleepForTimeInterval:interval];
            continue;
        }
        
        @autoreleasepool {
            NSData* pktData;
            if([videoQueue count] > packetLimit) {
                int frameType = FRAME_TYPE_UNKNOW;
                while(pktData == nil || [videoQueue count] > packetLimit) {
                    NSData *current = [videoQueue dequeue];
                    frameType = CheckH264IorP((void*)current.bytes, current.length);
                    if (frameType == FRAME_TYPE_I) {
                        pktData = current;
                    }
                    current = nil;
                    if([videoQueue count] == 0) break;
                }
//                NSLog(@"CH %i AFTER REDUCE TO I: %i", self.index, [videoQueue count]);
            } else {
                pktData = [videoQueue dequeue];
            }
            
            if (pktData == nil) {
                [NSThread sleepForTimeInterval:interval];
                continue;
            }
            //NSLog(@"data = %@", pktData);
            
            AVPacket pkt;
            pkt.data = (void*)pktData.bytes;
            pkt.size = (int)pktData.length;
            pktData = nil;
            
            seiDec(pkt.data+4, &pkt.size, &gpsdata);
            
            av_init_packet(&pkt);
            int got_gop = 0;
            
            pFrame = av_frame_alloc();
            avcodec_decode_video2(pVideoCodecCtx, pFrame, &got_gop, &pkt);
            av_free_packet(&pkt);
            
            if (got_gop <= 0 || !pFrame->data[0]) {
                [NSThread sleepForTimeInterval:interval];
                continue;
            }
            
            sws_scale(pSwsCtx, (const uint8_t *const *)pFrame->data, pFrame->linesize,
                      0, pVideoCodecCtx->height, picture.data, picture.linesize);
            av_frame_free(&pFrame);
            
            CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
            CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, picture.data[0], picture.linesize[0]*outputHeight,kCFAllocatorNull);
            CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            CGImageRef cgImage = CGImageCreate(outputWidth,
                                               outputHeight,
                                               8,
                                               24,
                                               picture.linesize[0],
                                               colorSpace,
                                               bitmapInfo,
                                               provider,
                                               NULL,
                                               NO,
                                               kCGRenderingIntentDefault);
            CGColorSpaceRelease(colorSpace);
            UIImage *last = self.frame;
            UIImage *newframe = [UIImage imageWithCGImage:cgImage];
            self.frame = newframe;
            last = nil;
            newframe = nil;
            CGImageRelease(cgImage);
            CGDataProviderRelease(provider);
            CFRelease(data);
            
            frameCount++;
            
            [NSThread sleepForTimeInterval:interval];
        }
    }
    NSLog(@"%i doDecode finish", self.index);
    [self clearQueue];
    NSLog(@"%i queue clear", self.index);
    [decodeThreadLock unlockWithCondition:DONE];
}
-(void)clearQueue{
    while([videoQueue count] > 0){
        NSData * pktData = [videoQueue dequeue];
        AVPacket pkt;
        pkt.data = (void*)pktData.bytes;
        pkt.size = (int)pktData.length;
        pktData = nil;
        av_init_packet(&pkt);
        av_free_packet(&pkt);
    }
    while([audioQueue count] > 0){
        NSData * pktData = [audioQueue dequeue];
        AVPacket pkt;
        pkt.data = (void*)pktData.bytes;
        pkt.size = (int)pktData.length;
        pktData = nil;
        av_init_packet(&pkt);
        av_free_packet(&pkt);
    }
}
-(void)setupAudioDecoder {
    /*
     if (audioStream >= 0) {
     _audioBufferSize = AVCODEC_MAX_AUDIO_FRAME_SIZE;
     _audioBuffer = av_malloc(_audioBufferSize);
     _inBuffer = NO;
     
     //_audioCodecContext = pFormatCtx->streams[audioStream]->codec;
     //_audioStream = pFormatCtx->streams[audioStream];
     
     //AVCodec *codec = avcodec_find_decoder(_audioCodecContext->codec_id);
     //if (codec == NULL) {
     //NSLog(@"Not found audio codec.");
     //return;
     //}
     
     //if (avcodec_open2(_audioCodecContext, codec, NULL) < 0) {
     //NSLog(@"Could not open audio codec.");
     //return;
     //}
     
     //if (audioPacketQueue) {
     //[audioPacketQueue release];
     //audioPacketQueue = nil;
     //}
     //audioPacketQueue = [[NSMutableArray alloc] init];
     
     //if (audioPacketQueueLock) {
     //[audioPacketQueueLock release];
     //audioPacketQueueLock = nil;
     //}
     //audioPacketQueueLock = [[NSLock alloc] init];
     
     } else {
     pFormatCtx->streams[audioStream]->discard = AVDISCARD_ALL;
     audioStream = -1;
     }*/
}
-(void)setupScaler {
    // Release old picture and scaler
    avpicture_free(&picture);
    sws_freeContext(pSwsCtx);
    
    // Allocate RGB picture
    avpicture_alloc(&picture, PIX_FMT_RGB24, outputWidth, outputHeight);
    
    // Setup scaler
    static int sws_flags =  SWS_FAST_BILINEAR;
    pSwsCtx = sws_getContext(pVideoCodecCtx->width,
                                     pVideoCodecCtx->height,
                                     pVideoCodecCtx->pix_fmt,
                                     outputWidth,
                                     outputHeight,
                                     PIX_FMT_RGB24,
                                     sws_flags, NULL, NULL, NULL);
    
}

-(int)decodeInterruptCB{
    return isRunningStreamThread;
}

-(NSString*)formatTimeParam:(NSDate*)stamp{
    NSDateFormatter *formatter;
    NSString        *dateString;

    formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyyMMdd_HHmm00"];
    dateString = [formatter stringFromDate:stamp];
    return dateString;
}

@end
