//
//  AboutViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *info;
-(IBAction) dismiss;
@end

@implementation AboutViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    NSLog(@"About Loaded... info: %f, %f",
          self.info.center.x,
          self.info.center.y);
}

- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
