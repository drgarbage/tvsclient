//
//  ConnectionEditorTableViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "ConnectionEditorTableViewController.h"
#import "MasterTableViewController.h"
#import "AppModel.h"

@interface ConnectionEditorTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *URL;
@property (weak, nonatomic) IBOutlet UITextField *UserName;
@property (weak, nonatomic) IBOutlet UITextField *Password;
- (IBAction)connect;
- (IBAction)dismiss;
@end

@implementation ConnectionEditorTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.SelectedDevice != nil) {
        self.URL.text  = self.SelectedDevice.URL;
        self.UserName.text = self.SelectedDevice.UserName;
        self.Password.text = self.SelectedDevice.Password;
    }
}

- (void)connect{
    Device *device = (self.SelectedDevice != nil)?
        self.SelectedDevice : [AppModel newDevice];
    
    device.URL = self.URL.text;
    device.UserName = self.UserName.text;
    device.Password = self.Password.text;
    
    
    AppModel *app = [AppModel sharedModel];
    if(![app.StoredDevices containsObject:device])
        [app.StoredDevices addObject:device];
    app.CurrentDevice = device;
    [app save];
    [self dismissViewControllerAnimated:YES completion:^(void){
        [MasterTableViewController showLive];
    }];
}

- (void)dismiss{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
