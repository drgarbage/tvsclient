//
//  RTSPVideoServer.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/13.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#define FRAME_TYPE_UNKNOW  0x00 /**< Type unknow */
#define FRAME_TYPE_I       0x01 /**< I Frame */
#define FRAME_TYPE_P       0X02 /**< P Frame */

typedef uint32_t DWORD;
typedef uint8_t BYTE;

#define FRAME_TYPE_UNKNOW  0x00 /**< Type unknow */
#define FRAME_TYPE_I       0x01 /**< I Frame */
#define FRAME_TYPE_P       0X02 /**< P Frame */

//static int CheckMpeg4IorP (BYTE* pRawData, int Datalen)
//{
//    int i = 0;
//    char cIorP;
//    DWORD dwB6 = 0xB6010000;
//    do
//    {
//        if( memcmp( &pRawData[i], &dwB6, 4 ) == 0 )
//        {
//            cIorP = pRawData[i+4];
//            cIorP &= 0xC0;
//            if( cIorP == 64 ) // PFrame
//                return FRAME_TYPE_P;
//            if( cIorP == 0 )  // IFrame
//                return FRAME_TYPE_I;
//        }
//        
//    } while( Datalen - 4 > i++ );
//    return FRAME_TYPE_UNKNOW;
//}
//
//static int CheckH264IorP (BYTE* pData, DWORD dwLen)
//{
//    
//    unsigned long nSize = dwLen;
//    if( nSize < 4 )
//        return FRAME_TYPE_UNKNOW;
//    
//    while( (4 <= nSize) && ((0 != pData[0]) || (0 != pData[1]) || (1 != pData[2]) ) )
//    {
//        pData += 1;
//        nSize -= 1;
//    }
//    
//    if( 4 <= nSize )
//    {
//        int n = ((pData[0] << 24) | (pData[1] << 16) | (pData[2] << 8) | (pData[3])) & 0x1f;
//        
//        switch( n )
//        {
//            case 2://NAL_UT_DPA
//            case 3://NAL_UT_DPB
//            case 4://NAL_UT_DPC
//            case 5://NAL_UT_IDR_SLICE:
//            case 7:
//            case 8:
//                return FRAME_TYPE_I;
//                break;
//        }
//    }
//    return FRAME_TYPE_P;
//}

@protocol RTSPVideoServerDelegate <NSObject>
-(void)didUpdateFrame;
@end

@interface RTSPVideoServer : NSObject
@property (nonatomic, retain) id<RTSPVideoServerDelegate> delegate;
@property (nonatomic, retain) NSString *host;
@property (nonatomic, weak, readonly) NSString *status;
@property (nonatomic, retain, readonly) UIImage *frame;
@property (nonatomic, assign) NSInteger speed;
@property (nonatomic, assign) NSInteger channel;
-(void)play:(NSDate*)stamp;
-(void)pause;
-(void)stop;
-(void)mute;
-(void)unmute;
@end