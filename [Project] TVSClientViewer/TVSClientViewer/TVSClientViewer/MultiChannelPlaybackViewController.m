//
//  MultiChannelPlaybackViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/13.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "VideoView.h"
#import "AppModel.h"
#import "MultiChannelPlaybackViewController.h"
#import "TimeSelectorViewController.h"
//#import "SingleChannelPlaybackViewController.h"


@interface MultiChannelPlaybackViewController () <UIScrollViewDelegate, TimeSelectorViewControllerDelegate> {
    NSMutableArray* videoviews;
    NSMutableArray* toolbarForPlaying;
    NSMutableArray* toolbarForPaused;
    NSTimer *timer;
    NSDate *selectedDate;
    int selectedPage;

}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnPlay;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnPause;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSpeed;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLiveInfo;
@property (weak, nonatomic) IBOutlet UIToolbar *tbPlayback;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIDatePicker *pickerView;
@property (weak, nonatomic) TimeSelectorViewController *timevc;
-(IBAction)play:(id)sender;
-(IBAction)pause:(id)sender;
-(IBAction)backward:(id)sender;
-(IBAction)forward:(id)sender;
@end

@implementation MultiChannelPlaybackViewController

- (void)play:(id)sender{
    [self doplay];
}
- (void)pause:(id)sender{
    [self dopause];
}
- (void)backward:(id)sender{
    [self dobackward];
}
- (void)forward:(id)sender{
    [self doforward];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"MultiChannelPlaybackViewController viewDidLoad");
    
    
    videoviews = [NSMutableArray array];
    toolbarForPlaying = [self.tbPlayback.items mutableCopy];
    toolbarForPaused = [self.tbPlayback.items mutableCopy];
    [toolbarForPlaying removeObject:self.btnPlay];
    [toolbarForPaused removeObject:self.btnPause];
    [self updateStatus];
    
    // config views
    for(int i = 0; i < 8; i++) {
        UIView *view = [self videoView];
        [self.scrollView addSubview:view];
        [videoviews addObject:view];
    }
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    [[AppModel sharedModel].CurrentDevice freeMemory];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self arrangeVideoView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                             target:self
                                           selector:@selector(refresh:)
                                           userInfo:nil
                                            repeats:YES];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self dostop];
    NSLog(@"MultiChannelPlaybackViewController Disappear");
}

- (void)doplay{
    Device *device = [AppModel sharedModel].CurrentDevice;
    if([self currentPage] == 0) {
        [((Channel*)device.channels[0]) play: selectedDate];
        [((Channel*)device.channels[1]) play: selectedDate];
        [((Channel*)device.channels[2]) play: selectedDate];
        [((Channel*)device.channels[3]) play: selectedDate];
        [((Channel*)device.channels[4]) stop];
        [((Channel*)device.channels[5]) stop];
        [((Channel*)device.channels[6]) stop];
        [((Channel*)device.channels[7]) stop];
    }else{
        [((Channel*)device.channels[0]) stop];
        [((Channel*)device.channels[1]) stop];
        [((Channel*)device.channels[2]) stop];
        [((Channel*)device.channels[3]) stop];
        [((Channel*)device.channels[4]) play: selectedDate];
        [((Channel*)device.channels[5]) play: selectedDate];
        [((Channel*)device.channels[6]) play: selectedDate];
        [((Channel*)device.channels[7]) play: selectedDate];
    }
    [self updateStatus];
}
- (void)dopause{
    Device *device = [AppModel sharedModel].CurrentDevice;
    [((Channel*)device.channels[0]) pause];
    [((Channel*)device.channels[1]) pause];
    [((Channel*)device.channels[2]) pause];
    [((Channel*)device.channels[3]) pause];
    [((Channel*)device.channels[4]) pause];
    [((Channel*)device.channels[5]) pause];
    [((Channel*)device.channels[6]) pause];
    [((Channel*)device.channels[7]) pause];
    [self updateStatus];
}
- (void)dostop{
    Device *device = [AppModel sharedModel].CurrentDevice;
    [((Channel*)device.channels[0]) stop];
    [((Channel*)device.channels[1]) stop];
    [((Channel*)device.channels[2]) stop];
    [((Channel*)device.channels[3]) stop];
    [((Channel*)device.channels[4]) stop];
    [((Channel*)device.channels[5]) stop];
    [((Channel*)device.channels[6]) stop];
    [((Channel*)device.channels[7]) stop];
    [self updateStatus];
}
- (void)doforward{
    NSLog(@"DO FORWARD");
    Device *device = [AppModel sharedModel].CurrentDevice;
    if([self currentPage] == 0) {
        [((Channel*)device.channels[0]) forward];
        [((Channel*)device.channels[1]) forward];
        [((Channel*)device.channels[2]) forward];
        [((Channel*)device.channels[3]) forward];
    }else{
        [((Channel*)device.channels[4]) forward];
        [((Channel*)device.channels[5]) forward];
        [((Channel*)device.channels[6]) forward];
        [((Channel*)device.channels[7]) forward];
    }
    [self updateStatus];
}
- (void)dobackward{
    NSLog(@"DO BACKWARD");
    Device *device = [AppModel sharedModel].CurrentDevice;
    if([self currentPage] == 0) {
        [((Channel*)device.channels[0]) backward];
        [((Channel*)device.channels[1]) backward];
        [((Channel*)device.channels[2]) backward];
        [((Channel*)device.channels[3]) backward];
    }else{
        [((Channel*)device.channels[4]) backward];
        [((Channel*)device.channels[5]) backward];
        [((Channel*)device.channels[6]) backward];
        [((Channel*)device.channels[7]) backward];
    }
    [self updateStatus];
}
- (int)currentPage{
    CGFloat pageHeight = self.scrollView.frame.size.height;
    return floor((self.scrollView.contentOffset.y - pageHeight / 2) / pageHeight) + 1;
}
- (void)updateStatus{
    Device *device = [AppModel sharedModel].CurrentDevice;
    Channel *channel = device.channels[2];
    if([self currentPage] != 0) {
        channel = device.channels[4];
    }
    
    NSString *info = channel.LiveInfo;
    [self showSpeed:channel.Speed];
    if(info != nil)
        [self.btnLiveInfo setTitle:info];
    [self.tbPlayback setItems:
        (channel.status == STATUS_PAUSED)?
            toolbarForPaused:
            toolbarForPlaying];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self doplay];
    [self updateStatus];
}
- (void)arrangeVideoView {
    CGRect frame = self.scrollView.frame;
    
    float totalWidth = frame.size.width;
    float totalHeight = frame.size.height;
    float videoWidth = totalWidth / 2;
    float videoHeight = totalHeight / 2;
    int index = 0;
    for(VideoView *view in videoviews) {
        float x = (index % 2 == 0) ? 0 : videoWidth;
        float y = (index / 2) * videoHeight;
        view.frame = CGRectMake(x, y, videoWidth, videoHeight);
        index++;
    }
    
    self.scrollView.contentSize = CGSizeMake(totalWidth, totalHeight*2);
}

- (UIView*) videoView {
    CGRect defaultFrame = CGRectMake(0, 0, 640, 480);
    
//    UITapGestureRecognizer *gesture =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleTapGesture:)];
//    gesture.numberOfTapsRequired = 2;
    
    ChannelView *videoView = [[ChannelView alloc] initWithFrame:defaultFrame];
    videoView.layer.borderColor = [UIColor whiteColor].CGColor;
    videoView.layer.borderWidth = 1;
    [videoView setBackgroundColor:[UIColor blackColor]];
    [videoView setUserInteractionEnabled:YES];
//    [videoView addGestureRecognizer: gesture];
//    videoView.gesture = gesture;
    
    return videoView;
}
- (void)refresh:(id)state {
    int page = [self currentPage];
    int start = page * 4;
    int end = start + 4;
    Device *device = [AppModel sharedModel].CurrentDevice;
    Channel *activeChannel = nil;
    for (int index = start; index < end; index++) {
        @autoreleasepool {
            Channel *channel = [device.channels objectAtIndex:index];
            if(!channel.isFrameReady)continue;
            UIImageView *view = [videoviews objectAtIndex:index];
            UIImage *img = channel.frame;
            
            if(channel.isFailConnection){
                [view stopAnimating];
                continue;
            }
            
            if(!channel.isFrameReady || img == nil){
                [view startAnimating];
            } else {
                [view stopAnimating];
                UIImage *old = view.image; old = nil;
                view.image = img;
            }
//            if(img == nil) continue;
//            UIImage *old = view.image; old = nil;
//            view.image = img;
//            activeChannel = channel;
        }
    }
    
    if(activeChannel==nil)return;
    [self.btnLiveInfo setTitle:activeChannel.LiveInfo];
    [self.tbPlayback setItems:
     (activeChannel.status == STATUS_PAUSED)?
         toolbarForPaused:
         toolbarForPlaying];
}
- (void)handleTapGesture:(UITapGestureRecognizer *)sender{
    if (sender.state == UIGestureRecognizerStateRecognized) {
        for(int index = 0; index < videoviews.count; index++) {
            ChannelView *view = [videoviews objectAtIndex:index];
            if(view.gesture == sender) {
                selectedPage = index;
                [self dostop];
                [self performSegueWithIdentifier:@"single" sender:self];
                break;
            }
        }
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    if([segue.identifier isEqualToString:@"single"]) {
//        SingleChannelPlaybackViewController *vc = segue.destinationViewController;
//        vc.startPage = selectedPage;
//    }
    
    self.pickerView = nil;
    if([segue.identifier isEqualToString:@"time"]) {
        self.timevc = segue.destinationViewController;
        self.timevc.delegate = self;
    }
}
- (void)didUpdateTime{
    [self dostop];
    selectedDate = self.timevc.datePicker.date;
    [self doplay];
    [self updateStatus];
}
- (void)showSpeed: (NSInteger) speed {
    [self.btnSpeed setTitle:[NSString stringWithFormat:@"%ix", speed]];
}
@end
