//
//  StreamThread.h
//  TVSClientViewer
//
//  Created by Johnson on 2016/1/20.
//  Copyright © 2016年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface StreamThread : NSObject
@property (nonatomic, assign) int index;
@property (nonatomic, readonly, assign) BOOL isContinueStreaming;
@property (nonatomic, readonly, assign) BOOL isFrameReady;
@property (nonatomic, readonly, assign) BOOL isFailConnection;
@property (nonatomic, retain) UIImage* frame;
-(void)startStream:(NSString*) url stamp: (NSDate*) playbackStamp;
-(void)dropStream;
-(void)playStream: (int) speed;
-(void)pauseStream;
@end
