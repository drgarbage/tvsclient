//
//  VideoDevice.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/11.
//  Copyright (c) 2015年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Device.h"

@interface VideoDevice : Device
@end

//@protocol ChannelDelegate <NSObject>
//-(void)didUpdateFrame;
//@end

@interface VideoChannel : Channel
//@property (nonatomic, retain) id<ChannelDelegate> delegate;
//@property (nonatomic, retain) VideoDevice *device;
//@property (nonatomic, assign) NSInteger index;
//@property (nonatomic, retain) UIImage *frame;
//@property (nonatomic, assign, readonly) NSInteger status;
//@property (nonatomic, assign) NSInteger Speed;
//@property (nonatomic, retain, readonly) NSString *LiveInfo;
//@property (nonatomic, assign, readonly) BOOL isContinueStreaming;
//@property (nonatomic, assign, readonly) BOOL isFrameReady;
+(id)channelOfDevice: (Device*) device withIndex: (NSInteger) index;
//-(void)play:(NSDate*)stamp;
//-(void)forward;
//-(void)backward;
//-(void)pause;
//-(void)stop;
//-(void)mute;
//-(void)unmute;
//-(void)freeMemory;
@end
