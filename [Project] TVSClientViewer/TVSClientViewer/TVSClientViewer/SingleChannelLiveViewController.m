//
//  SingleChannelLiveViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "SingleChannelLiveViewController.h"
#import "AppModel.h"

@interface SingleChannelLiveViewController () <UIScrollViewDelegate> {
    NSMutableArray *videoviews;
    NSTimer *timer;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation SingleChannelLiveViewController
- (IBAction)backward:(id)sender {
    NSLog(@"backward");
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    videoviews = [NSMutableArray array];
    
    // config views
    for(int i = 0; i < 8; i++) {
        UIView *view = [self videoView];
        [self.scrollView addSubview:view];
        [videoviews addObject:view];
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                             target:self
                                           selector:@selector(refresh:)
                                           userInfo:nil
                                            repeats:YES];
}
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    [[AppModel sharedModel].CurrentDevice freeMemory];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self arrangeVideoView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self pageTo:self.startPage];
    [self doplay];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self dostop];
    NSLog(@"SingleChannelLiveViewController Disappear");
}

- (void)doplay{
    Device *device = [AppModel sharedModel].CurrentDevice;
    
    int page = [self currentPage];
    
    NSLog(@"Play Index: %i", page);
    
    for(int index = 0; index < device.channels.count; index++) {
        if(index == page)
            [((Channel*)device.channels[index]) play: nil];
        else
            [((Channel*)device.channels[index]) stop];
    }
}
- (void)dopause{
    Device *device = [AppModel sharedModel].CurrentDevice;
    for(int index = 0; index < device.channels.count; index++) {
        [((Channel*)device.channels[index]) pause];
    }
}
- (void)dostop{
    Device *device = [AppModel sharedModel].CurrentDevice;
    for(int index = 0; index < device.channels.count; index++) {
        [((Channel*)device.channels[index]) stop];
    }
}
- (int)currentPage{
    CGFloat pageHeight = self.scrollView.frame.size.height;
    return floor((self.scrollView.contentOffset.y - pageHeight / 2) / pageHeight) + 1;
}
- (void)pageTo: (NSInteger) index{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    CGFloat pageHeight = self.scrollView.frame.size.height;
    [self.scrollView scrollRectToVisible: CGRectMake(0, pageHeight*index, pageWidth, pageHeight)
                                animated: NO];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self doplay];
}
- (void)arrangeVideoView {
    CGRect frame = self.scrollView.frame;
    
    float totalWidth = frame.size.width;
    float totalHeight = frame.size.height;
    float videoWidth = totalWidth;
    float videoHeight = totalHeight;
    int index = 0;
    for(UIView *view in videoviews) {
        float y = index * videoHeight;
        view.frame = CGRectMake(0, y, videoWidth, videoHeight);
        index++;
    }
    
    self.scrollView.contentSize = CGSizeMake(totalWidth, totalHeight*8);
}

- (UIView*) videoView {
    CGRect defaultFrame = CGRectMake(0, 0, 640, 480);
    UIView *videoView = [[UIImageView alloc] initWithFrame:defaultFrame];
    [videoView setBackgroundColor:[UIColor blackColor]];
    videoView.layer.borderColor = [UIColor whiteColor].CGColor;
    videoView.layer.borderWidth = 1;
    
    return videoView;
}
- (void)refresh:(id)state {
    int page = [self currentPage];
    Device *device = [AppModel sharedModel].CurrentDevice;
    @autoreleasepool {
        Channel *channel = [device.channels objectAtIndex:page];
        if(!channel.isFrameReady)return;
        UIImageView *view = [videoviews objectAtIndex:page];
        UIImage *img = channel.frame;
//        if(img == nil) return;
//        UIImage *old = view.image; old = nil;
//        view.image = img;
//        Channel *channel = [device.channels objectAtIndex:index];
//        if(!channel.isFrameReady)continue;
//        UIImageView *view = [videoviews objectAtIndex:index];
//        UIImage *img = channel.frame;
        
        if(channel.isFailConnection){
            [view stopAnimating];
            return;
        }
        
        if(!channel.isFrameReady || img == nil){
            [view startAnimating];
        } else {
            [view stopAnimating];
            UIImage *old = view.image; old = nil;
            view.image = img;
        }
    }
}
@end
