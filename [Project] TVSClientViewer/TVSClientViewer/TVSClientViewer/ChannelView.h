//
//  ChannelView.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/25.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChannelView : UIImageView
@property (nonatomic, retain) UITapGestureRecognizer *gesture;
-(void)startAnimating;
-(void)stopAnimating;
@end