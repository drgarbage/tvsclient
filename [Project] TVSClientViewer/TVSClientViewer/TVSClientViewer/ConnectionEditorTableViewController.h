//
//  ConnectionEditorTableViewController.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppModel.h"

@interface ConnectionEditorTableViewController : UITableViewController
@property (nonatomic, retain) Device* SelectedDevice;
@end
