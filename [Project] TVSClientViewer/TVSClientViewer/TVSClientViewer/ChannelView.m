//
//  ChannelView.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/25.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "ChannelView.h"
@interface ChannelView(){
    UIActivityIndicatorView *loading;
}
@end
@implementation ChannelView
-(void)initialize{
    loading = [[UIActivityIndicatorView alloc] init];
    loading.frame = self.frame;
    [loading startAnimating];
    [self addSubview:loading];
}
-(instancetype)init{
    self = [super init];
    if(self)
        [self initialize];
    return self;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self)
        [self initialize];
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self)
        [self initialize];
    return self;
}
-(instancetype)initWithImage:(UIImage *)image{
    self = [super initWithImage:image];
    if(self)
        [self initialize];
    return self;
}
-(instancetype)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage{
    self = [super initWithImage:image highlightedImage:highlightedImage];
    if(self)
        [self initialize];
    return self;
}
-(void)startAnimating{
    [loading startAnimating];
}
-(void)stopAnimating{
    [loading stopAnimating];
}
-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    loading.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
}
@end