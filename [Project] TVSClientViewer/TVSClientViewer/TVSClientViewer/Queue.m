//
//  Queue.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/20.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "Queue.h"

//@interface Queue ()
//@property (nonatomic, retain) NSMutableArray *ary;
//@end

@implementation Queue

- (id)init{
    self = [super init];
    if (self) {
        ary = [NSMutableArray new];
    }
    return self;
}
// Queues are first-in-first-out, so we remove objects from the head
- (id) dequeue {
    @synchronized(ary) {
        @autoreleasepool {
            if (ary.count == 0) return nil; // to avoid raising exception (Quinn)
            id headObject = [ary firstObject];
            if (headObject != nil) {
                [ary removeObject:headObject];
            }
            return headObject;
        }
    }
}
- (NSInteger)count{
    return ary.count;
}

// Add to the tail of the queue (no one likes it when people cut in line!)
- (void) enqueue:(id)anObject {
    @synchronized(ary) {
        [ary addObject:anObject];
    }
}
- (void) clear{
    @synchronized(ary) {
        @autoreleasepool {
            [ary removeAllObjects];
        }
    }
}
- (void) drop:(int)amount{
    @synchronized(ary) {
        int target = fmin(amount, ary.count);
        while (target > 0) {
            target--;
            [ary removeObjectAtIndex:0];
        }
    }
}
@end