//
//  main.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/11.
//  Copyright (c) 2015年 tvs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
