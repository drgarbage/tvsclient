//
//  ViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/11.
//  Copyright (c) 2015年 tvs. All rights reserved.
//

#import "ViewController.h"
#import "RTSPVideoServer.h"

@implementation ViewController
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController popToRootViewControllerAnimated:NO];
}
@end