//
//  DevicesTableViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/21.
//  Copyright (c) 2015年 tvs. All rights reserved.
//

#import "DevicesTableViewController.h"
#import "ConnectionEditorTableViewController.h"
#import "MasterTableViewController.h"
#import "UPnPManager.h"
#import "AppModel.h"


@interface DevicesTableViewController ()<UPnPDBObserver> {
    NSMutableArray *_foundDevices; //BasicUPnPDevice*
    Device *_selectedDevice;
    
}
-(IBAction)addDevice;
-(IBAction)scan;
-(IBAction)dismiss;
@end

@implementation DevicesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    @autoreleasepool {
        _foundDevices = [NSMutableArray new];
    }
    
    [[[UPnPManager GetInstance] DB] addObserver:self];
    [[[UPnPManager GetInstance] SSDP] setUserAgentProduct:@"upnpxdemo/1.0" andOS:@"OSX"];
    
    [self scan];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self stopscan];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self stopscan];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // 0: Stored Devices
    // 1: Found Devices
    // 2: Button
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [AppModel sharedModel].StoredDevices.count;
            break;
        case 1:
            return (_foundDevices.count>0) ? _foundDevices.count : 1;
            break;
        case 2:
            return 1;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"ScanButton";
    switch (indexPath.section) {
        case 0: cellIdentifier = @"StoredDevices"; break;
        case 1: cellIdentifier = (_foundDevices.count>0)?@"FoundDevices":@"Scanning"; break;
        case 2: cellIdentifier = @"ScanButton"; break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    switch (indexPath.section) {
        case 0:
        {
            Device *device = [AppModel sharedModel].StoredDevices[indexPath.row];
            cell.textLabel.text = device.URL;
            break;
        }
        case 1:
        {
            if(_foundDevices.count > 0){
                BasicUPnPDevice *device = _foundDevices[indexPath.row];
                cell.textLabel.text = [NSString stringWithFormat:@"%@",[device friendlyName]];//[device baseURL].host];
            }
            break;
        }
        default:
            break;
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[AppModel sharedModel].StoredDevices removeObjectAtIndex:indexPath.row];
        [[AppModel sharedModel] save];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            // todo: connect device
            _selectedDevice = [[AppModel sharedModel].StoredDevices objectAtIndex:indexPath.row];
            [self connect];
            break;
        }
        case 1:
        {
            if(_foundDevices.count == 0)
                return;
            // todo: edit as new connection
            BasicUPnPDevice *upnpDevice = _foundDevices[indexPath.row];

            Device *device = [AppModel newDeviceWithURL:[upnpDevice.baseURL host]
                                                   user:@""
                                               password:@""];
            _selectedDevice = device;
            [self performSegueWithIdentifier:@"EditConnection" sender:self];
            break;
        }
        case 2:
            [self scan];
            break;
    }
}
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section != 0) return;
    _selectedDevice = [[AppModel sharedModel].StoredDevices objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"EditConnection" sender:self];
}
- (void)connect{
    if(!_selectedDevice) return;
    [AppModel sharedModel].CurrentDevice = _selectedDevice;
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        [MasterTableViewController showLive];
    }];
}
- (void)scan{
    //Search for UPnP Devices
    [[[UPnPManager GetInstance] SSDP] searchSSDP];
    //[[[UPnPManager GetInstance] SSDP] searchForMediaServer];
}
- (void)stopscan{
    [[[UPnPManager GetInstance] SSDP] stopSSDP];
}
- (void)dismiss{
    [self stopscan];
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"AddNewConnection"] ||
        [segue.identifier isEqualToString:@"EditConnection"]) {
        ConnectionEditorTableViewController *ctrl =
            (ConnectionEditorTableViewController*)
                segue.destinationViewController;
        ctrl.SelectedDevice = _selectedDevice;
    }
}

- (void)addDevice{
    _selectedDevice = [AppModel newDevice];
    [self performSegueWithIdentifier:@"EditConnection" sender:self];
}
- (void)editDevice: (id) device {
    
}


#pragma mark - protocol UPnPDBObserver

-(void)UPnPDBWillUpdate:(UPnPDB*)sender{
    NSLog(@"UPnPDBWillUpdate %lu", (unsigned long)[_foundDevices count]);
}

-(void)UPnPDBUpdated:(UPnPDB*)sender{
    @autoreleasepool {
        UPnPDB* db = [[UPnPManager GetInstance] DB];
        NSMutableArray * newFoundDevices;
        for (BasicUPnPDevice * device in db.rootDevices) {
            if([[device friendlyName] hasPrefix:@"TN"])
                [newFoundDevices addObject:device];
        }
        _foundDevices = newFoundDevices;
        newFoundDevices = nil;
    }
    NSLog(@"UPnPDBUpdated %lu", (unsigned long)[_foundDevices count]);
    [self.tableView performSelectorOnMainThread : @ selector(reloadData) withObject:nil waitUntilDone:YES];
}
@end
