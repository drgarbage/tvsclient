//
//  SingleChannelLiveViewController.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleChannelLiveViewController : UIViewController
@property (nonatomic, assign) NSInteger startPage;
-(void)pageTo: (int) index;
@end
