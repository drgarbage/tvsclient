//
//  Queue.h
//  TVSClientViewer
//
//  Created by Johnson on 2015/11/20.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Queue : NSObject {
    NSMutableArray *ary;
}
@property (nonatomic, assign, readonly) NSInteger count;
- (id) dequeue;
- (void) enqueue:(id)obj;
- (void) clear;
- (void) drop: (int) amount;
@end