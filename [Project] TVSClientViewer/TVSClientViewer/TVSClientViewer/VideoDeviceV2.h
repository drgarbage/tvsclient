//
//  VideoDeviceV2.h
//  TVSClientViewer
//
//  Created by Johnson on 2016/1/20.
//  Copyright © 2016年 tvs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"

@interface VideoDeviceV2 : Device
@end

@interface ChannelV2 : Channel
+(id)channelOfDevice: (Device*) device withIndex: (NSInteger) index;
@end
