//
//  MultiChannelLiveViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "MultiChannelLiveViewController.h"
#import "SingleChannelLiveViewController.h"
#import "VideoView.h"
#import "AppModel.h"
#import "VideoDeviceV2.h"

@interface MultiChannelLiveViewController () <UIScrollViewDelegate> {
    NSMutableArray *videoviews;
    NSTimer *timer;
    NSDate *lastPlay;
    int selectedPage;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation MultiChannelLiveViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"MultiChannelLiveViewController viewDidLoad");
    
    videoviews = [NSMutableArray array];
    
    // config views
    for(int i = 0; i < 8; i++) {
        UIView *view = [self videoView];
        [self.scrollView addSubview:view];
        [videoviews addObject:view];
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    [[AppModel sharedModel].CurrentDevice freeMemory];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self arrangeVideoView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self.navigationController setNavigationBarHidden:YES animated:NO];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self doplay];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                             target:self
                                           selector:@selector(refresh:)
                                           userInfo:nil
                                            repeats:YES];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self dostop];
    NSLog(@"MultiChannelLiveViewController Disappear");

}

- (void)doplay{
    Device *device = [AppModel sharedModel].CurrentDevice;
    lastPlay = [NSDate new];
    if([self currentPage] == 0) {
        [((Channel*)device.channels[0]) play: nil];
        [((Channel*)device.channels[1]) play: nil];
        [((Channel*)device.channels[2]) play: nil];
        [((Channel*)device.channels[3]) play: nil];
        [((Channel*)device.channels[0]) mute];
        [((Channel*)device.channels[1]) mute];
        [((Channel*)device.channels[2]) mute];
        [((Channel*)device.channels[3]) mute];
        [((Channel*)device.channels[4]) stop];
        [((Channel*)device.channels[5]) stop];
        [((Channel*)device.channels[6]) stop];
        [((Channel*)device.channels[7]) stop];
    }else{
        [((Channel*)device.channels[0]) stop];
        [((Channel*)device.channels[1]) stop];
        [((Channel*)device.channels[2]) stop];
        [((Channel*)device.channels[3]) stop];
        [((Channel*)device.channels[4]) play: nil];
        [((Channel*)device.channels[5]) play: nil];
        [((Channel*)device.channels[6]) play: nil];
        [((Channel*)device.channels[7]) play: nil];
        [((Channel*)device.channels[4]) mute];
        [((Channel*)device.channels[5]) mute];
        [((Channel*)device.channels[6]) mute];
        [((Channel*)device.channels[7]) mute];
    }
}
- (void)dopause{
    Device *device = [AppModel sharedModel].CurrentDevice;
    [((Channel*)device.channels[0]) pause];
    [((Channel*)device.channels[1]) pause];
    [((Channel*)device.channels[2]) pause];
    [((Channel*)device.channels[3]) pause];
    [((Channel*)device.channels[4]) pause];
    [((Channel*)device.channels[5]) pause];
    [((Channel*)device.channels[6]) pause];
    [((Channel*)device.channels[7]) pause];
}
- (void)dostop{
    Device *device = [AppModel sharedModel].CurrentDevice;
    [((Channel*)device.channels[0]) stop];
    [((Channel*)device.channels[1]) stop];
    [((Channel*)device.channels[2]) stop];
    [((Channel*)device.channels[3]) stop];
    [((Channel*)device.channels[4]) stop];
    [((Channel*)device.channels[5]) stop];
    [((Channel*)device.channels[6]) stop];
    [((Channel*)device.channels[7]) stop];
}
- (int)currentPage{
    CGFloat pageHeight = self.scrollView.frame.size.height;
    return floor((self.scrollView.contentOffset.y - pageHeight / 2) / pageHeight) + 1;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self doplay];
}
- (void)arrangeVideoView {
    CGRect frame = self.scrollView.frame;
    
    float totalWidth = frame.size.width;
    float totalHeight = frame.size.height;
    float videoWidth = totalWidth / 2;
    float videoHeight = totalHeight / 2;
    int index = 0;
    for(VideoView *view in videoviews) {
        float x = (index % 2 == 0) ? 0 : videoWidth;
        float y = (index / 2) * videoHeight;
        view.frame = CGRectMake(x, y, videoWidth, videoHeight);
        index++;
    }
    
    self.scrollView.contentSize = CGSizeMake(totalWidth, totalHeight*2);
}

- (UIView*) videoView {
    CGRect defaultFrame = CGRectMake(0, 0, 640, 480);

    UITapGestureRecognizer *gesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTapGesture:)];
    gesture.numberOfTapsRequired = 2;
    
    ChannelView *videoView = [[ChannelView alloc] initWithFrame:defaultFrame];
    videoView.layer.borderColor = [UIColor whiteColor].CGColor;
    videoView.layer.borderWidth = 1;
    [videoView setBackgroundColor:[UIColor blackColor]];
    [videoView setUserInteractionEnabled:YES];
    [videoView addGestureRecognizer: gesture];
    videoView.gesture = gesture;
    
    return videoView;
}
-(void)refresh:(id)state {
    int page = [self currentPage];
    int start = page * 4;
    int end = start + 4;
    @autoreleasepool {
        Device *device = [AppModel sharedModel].CurrentDevice;
        for (int index = start; index < end; index++) {
            @autoreleasepool {
                Channel *channel = [device.channels objectAtIndex:index];
                ChannelView *view = [videoviews objectAtIndex:index];
                UIImage *img = channel.frame;
                
                if(channel.isFailConnection){
                    [view stopAnimating];
                    continue;
                }
                
                if(!channel.isFrameReady || img == nil){
                    [view startAnimating];
                } else {
                    [view stopAnimating];
                    UIImage *old = view.image; old = nil;
                    view.image = img;
                }
            }
        }
    }
}
-(void)handleTapGesture:(UITapGestureRecognizer *)sender{
    if (sender.state == UIGestureRecognizerStateRecognized) {
        for(int index = 0; index < videoviews.count; index++) {
            ChannelView *view = [videoviews objectAtIndex:index];
            if(view.gesture == sender) {
                selectedPage = index;
                [self dostop];
                [self performSegueWithIdentifier:@"single" sender:self];
                break;
            }
        }
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"single"]) {
        SingleChannelLiveViewController *vc = segue.destinationViewController;
        vc.startPage = selectedPage;
    }
}
@end