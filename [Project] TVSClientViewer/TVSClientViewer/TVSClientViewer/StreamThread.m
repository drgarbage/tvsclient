//
//  StreamThread.m
//  TVSClientViewer
//
//  Created by Johnson on 2016/1/20.
//  Copyright © 2016年 tvs. All rights reserved.
//

#import "StreamThread.h"
#import "avformat.h"
#import "swscale.h"
#import "Queue.h"
#import "SeiEncDec.h"

#define DONE 1
#define NOTDONE 0
#define LIVE_STREAM_PATTERN @"rtsp://%@/ch%03ld2.sdp"
#define PLAYBACK_STREAM_PATTERN @"rtsp://%@/ch%03ld2_%@.dsk"

typedef uint32_t DWORD;
typedef uint8_t BYTE;

#define FRAME_TYPE_UNKNOW  0x00 /**< Type unknow */
#define FRAME_TYPE_I       0x01 /**< I Frame */
#define FRAME_TYPE_P       0X02 /**< P Frame */

// wait 5 sec beofore change channel

int decode_interrupt_cb(void *ctx){
    StreamThread *streamTrd = (__bridge StreamThread*)ctx;
    return (streamTrd.isContinueStreaming)?0:1;
};

static int CheckH264IorP (BYTE* pData, DWORD dwLen){
    
    unsigned long nSize = dwLen;
    if( nSize < 4 )
        return FRAME_TYPE_UNKNOW;
    
    while( (4 <= nSize) && ((0 != pData[0]) || (0 != pData[1]) || (1 != pData[2]) ) )
    {
        pData += 1;
        nSize -= 1;
    }
    
    if( 4 <= nSize )
    {
        int n = ((pData[0] << 24) | (pData[1] << 16) | (pData[2] << 8) | (pData[3])) & 0x1f;
        
        switch( n )
        {
            case 2://NAL_UT_DPA
            case 3://NAL_UT_DPB
            case 4://NAL_UT_DPC
            case 5://NAL_UT_IDR_SLICE:
            case 7:
            case 8:
                return FRAME_TYPE_I;
                break;
        }
    }
    return FRAME_TYPE_P;
}

@interface StreamThread (){
    NSString *url;
    NSDate *playStamp;
    //
    // FFmpeg variables
    //
    AVFormatContext *pFormatCtx;
    AVCodecContext *pVideoCodecCtx;
    AVCodecContext *pAudioCodecCtx;
    AVCodec *pVideoCodec;
    AVCodec *pAudioCodec;
    AVFrame *pFrame;
    //AVPacket packet;
    AVPicture picture;
    int videoStream;
    int audioStream;
    struct SwsContext *pSwsCtx;
    Queue *videoQueue;
    Queue *audioQueue;
    int outputWidth,
        outputHeight;
    
    //
    // Thread control
    //
    NSThread *streamThread;
    NSConditionLock *streamThreadLock;
    BOOL isRunningStreamThread;
    BOOL isPaused;
    BOOL isKilled;
    BOOL isFailed;
    
    NSThread *decodeThread;
    NSConditionLock *decodeThreadLock;
    BOOL isRunningDecodeThread;
    
    //
    // Stream Status Flags
    //
    BOOL
        is_avformat_open_input,
        is_avformat_find_stream_info,
        is_stream_id_found,
        is_avcodec_found,
        is_avcodec_video_open,
        is_avcodec_audio_open;
    
    //
    // counter
    //
    int frameCount;
}
@end

@implementation StreamThread
-(id)init{
    self = [super init];
    if(self){
        isPaused = FALSE;
        isRunningStreamThread = FALSE;
        isRunningDecodeThread = FALSE;
        isKilled = FALSE;
        isFailed = FALSE;
        av_register_all();
        videoQueue = [Queue new];
        audioQueue = [Queue new];
    }
    return self;
}
-(BOOL)isFrameReady{
    return frameCount > 30;
}
-(BOOL)isFailConnection{
    return isFailed;
}
//
// stream control
//
-(void)startStream:(NSString *)u stamp:(NSDate *)s{
    @autoreleasepool {
        url = u;
        playStamp = s;
    }
    NSThread *oneTimeTread =
        [[NSThread alloc] initWithTarget:self
                                selector:@selector(initStream:)
                                  object:nil];
    [oneTimeTread start];
}
-(void)dropStream{
    isKilled = TRUE;
    
    if(streamThread != nil){
        isRunningStreamThread = FALSE;
        [streamThreadLock lockWhenCondition:DONE];
        [streamThreadLock unlock];
        streamThreadLock = nil;
        
        [streamThread cancel];
        streamThread = nil;
    }

    if(decodeThread != nil){
        isRunningDecodeThread = FALSE;
        [decodeThreadLock lockWhenCondition:DONE];
        [decodeThreadLock unlock];
        decodeThreadLock = nil;
        
        [decodeThread cancel];
        decodeThread = nil;
    }
    
    [self doRelease];
}

//
// live control
//
-(void)playStream: (int) speed {
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            pFormatCtx->ts_id = speed;
            av_read_pause(pFormatCtx);
            av_read_play(pFormatCtx);
        }
    });
}
-(void)pauseStream {
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @synchronized(self) {
            av_read_pause(pFormatCtx);
        }
    });
}

//
// worker Threads
//
-(void)initStream:(id)state {
    @synchronized(self) {
        is_avformat_open_input = NO;
        is_avformat_find_stream_info = NO;
        is_stream_id_found = NO;
        is_avcodec_found = NO;
        is_avcodec_video_open = NO;
        is_avcodec_audio_open = NO;
        
        // Set the RTSP Options
        AVDictionary *opts = 0;
        av_dict_set(&opts, "rtsp_transport", "tcp", 0);
        
        if (!isKilled && avformat_open_input(&pFormatCtx, [url UTF8String], NULL, &opts) !=0 ) {
            av_log(NULL, AV_LOG_ERROR, "Couldn't open file\n");
            is_avformat_open_input = NO;
            goto Finish;
        } else {
            is_avformat_open_input = YES;
        }
        
        // Retrieve stream information
        if (!isKilled && avformat_find_stream_info(pFormatCtx,NULL) < 0) {
            av_log(NULL, AV_LOG_ERROR, "Couldn't find stream information\n");
            is_avformat_find_stream_info = NO;
            goto Finish;
        } else {
            is_avformat_find_stream_info = YES;
        }
        
        // Find the first video stream
        videoStream=-1; audioStream=-1;
        for (int i=0; i<pFormatCtx->nb_streams; i++) {
            if (pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) { videoStream=i; }
            if (pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO) { audioStream=i; }
        }
        if (videoStream==-1 && audioStream==-1) {
            is_stream_id_found = NO;
            goto Finish;
        } else {
            is_stream_id_found = YES;
        }
        
        AVIOInterruptCB int_cb = { decode_interrupt_cb, (__bridge void *)(self) };
        pFormatCtx->interrupt_callback = int_cb;
        
        //
        // Setup Video Codec
        //
        pVideoCodecCtx = pFormatCtx->streams[videoStream]->codec;
        pVideoCodec = avcodec_find_decoder(pVideoCodecCtx->codec_id);
        if (isKilled || pVideoCodec == NULL) { goto Finish; }
        if (isKilled || avcodec_open2(pVideoCodecCtx, pVideoCodec, NULL) < 0) {
            pVideoCodecCtx = NULL;
            is_avcodec_video_open = NO;
            goto Finish;
        } else {
            is_avcodec_video_open = YES;
        }
        
        //
        // Setup Audio Codec
        //
        //    if (audioStream <= 0) {
        //        //pFormatCtx->streams[audioStream]->discard = AVDISCARD_ALL;
        //        audioStream = -1;
        //    } else {
        //        pAudioCodecCtx = pFormatCtx->streams[audioStream]->codec;
        //        pAudioCodec = avcodec_find_decoder(pAudioCodecCtx->codec_id);
        //        if (pAudioCodec != NULL) {
        //            if (avcodec_open2(pAudioCodecCtx, pAudioCodec, NULL) < 0) {
        //                pAudioCodecCtx = NULL;
        //            } else {
        //                is_avcodec_audio_open = NO;
        //            }
        //        } else {
        //            is_avcodec_audio_open = NO;
        //        }
        //    }
        
        outputWidth = pVideoCodecCtx->width;
        outputHeight = pVideoCodecCtx->height;
        
        // @todo: enable later
        [self setupScaler];
        
        streamThreadLock = [[NSConditionLock alloc] initWithCondition:NOTDONE];
        isRunningStreamThread = TRUE;
        streamThread = [[NSThread alloc] initWithTarget:self selector:@selector(doStream:) object:nil];
        streamThread.threadPriority = 2;
        [streamThread start];
        
        decodeThreadLock = [[NSConditionLock alloc] initWithCondition:NOTDONE];
        isRunningDecodeThread = TRUE;
        decodeThread = [[NSThread alloc] initWithTarget:self selector:@selector(doDecode:) object:self];
        streamThread.threadPriority = 5;
        [decodeThread start];
        
        return;
        
    Finish:
        isFailed = TRUE;
        NSLog(@"CH %i PREPARE FAILED: %@", self.index, url);
        NSLog(@"CH %i is_avformat_open_input: %@", self.index, (is_avformat_open_input)?@"YES":@"NO");
        NSLog(@"CH %i is_avformat_find_stream_info: %@", self.index, (is_avformat_find_stream_info)?@"YES":@"NO");
        NSLog(@"CH %i is_stream_id_found: %@", self.index, (is_stream_id_found)?@"YES":@"NO");
        NSLog(@"CH %i is_avcodec_found: %@", self.index, (is_avcodec_found)?@"YES":@"NO");
        
        if(is_avcodec_found && pVideoCodecCtx != NULL) {
            avcodec_close(pVideoCodecCtx);
            is_avcodec_found = NO;
        }
        
        if(is_avcodec_audio_open && pAudioCodecCtx != NULL) {
            avcodec_close(pAudioCodecCtx);
            is_avcodec_audio_open = NO;
        }
        
        if(is_avformat_open_input && pFormatCtx!=NULL) {
            avformat_close_input(&pFormatCtx);
            is_avformat_open_input = NO;
        }
        
        if(pFrame != NULL){
            av_frame_free(&pFrame);
        }
            
    }
}
-(void)doStream: (id) state {
    NSLog(@"doReceive:[streamThreadLock lock]");
    [streamThreadLock lock];
    
    isRunningStreamThread = TRUE;
    
    while (!isKilled && isRunningStreamThread) {
        
        if(isPaused) {
            [NSThread sleepForTimeInterval:1.0/60.0];
            continue;
        }
        
        AVPacket packet;
        
        //@synchronized(self) {
            //NSLog(@"av_read_frame");
            if(pFormatCtx == NULL || av_read_frame(pFormatCtx, &packet)<0)
                continue;
        //}
        
        if(packet.stream_index == videoStream) {
            //NSLog(@"videoStream");
            @autoreleasepool {
                NSData * data = [NSData dataWithBytes:packet.data length:packet.size];
                [videoQueue enqueue:data];
                av_free_packet(&packet);
            }
        }
        
        if(packet.stream_index == audioStream) {
            NSLog(@"audioStream");
            @autoreleasepool {
                NSData * data = [NSData dataWithBytes:packet.data length:packet.size];
                [audioQueue enqueue:data];
                av_free_packet(&packet);
            }
        }
        
        [NSThread sleepForTimeInterval:1.0/60.0];
        //        NSLog(@"Packet...");
    }
    [streamThreadLock unlockWithCondition:DONE];
}
-(void)doDecode: (id) state {
    NSTimeInterval interval = 1.0/30.0;
    int packetLimit = 30;
    [decodeThreadLock lock];
    isRunningDecodeThread = TRUE;
    
    while(!isKilled && isRunningStreamThread){
        
        if(pVideoCodecCtx == NULL || pSwsCtx == NULL || videoQueue == nil) {
            [NSThread sleepForTimeInterval:interval];
            continue;
        }
        
        @autoreleasepool {
            NSData* pktData;
            if([videoQueue count] > packetLimit) {
                int frameType = FRAME_TYPE_UNKNOW;
                while(pktData == nil || [videoQueue count] > packetLimit) {
                    NSData *current = [videoQueue dequeue];
                    frameType = CheckH264IorP((void*)current.bytes, current.length);
                    if (frameType == FRAME_TYPE_I) {
                        pktData = current;
                    }
                    current = nil;
                    if([videoQueue count] == 0) break;
                }
                //                NSLog(@"CH %i AFTER REDUCE TO I: %i", self.index, [videoQueue count]);
            } else {
                pktData = [videoQueue dequeue];
            }
            
            if (pktData == nil) {
                [NSThread sleepForTimeInterval:interval];
                continue;
            }
            
            AVPacket pkt;
            pkt.data = (void*)pktData.bytes;
            pkt.size = (int)pktData.length;
            pktData = nil;
            
            //@todo: resume later
            //seiDec(pkt.data+4, &pkt.size, &gpsdata);
            
            av_init_packet(&pkt);
            int got_gop = 0;
            
            pFrame = av_frame_alloc();
            avcodec_decode_video2(pVideoCodecCtx, pFrame, &got_gop, &pkt);
            av_free_packet(&pkt);
            
            if (got_gop <= 0 || !pFrame->data[0]) {
                [NSThread sleepForTimeInterval:interval];
                continue;
            }
            
            sws_scale(pSwsCtx, (const uint8_t *const *)pFrame->data, pFrame->linesize,
                      0, pVideoCodecCtx->height, picture.data, picture.linesize);
            av_frame_free(&pFrame);
            
            CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
            CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, picture.data[0], picture.linesize[0]*outputHeight,kCFAllocatorNull);
            CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            CGImageRef cgImage = CGImageCreate(outputWidth,
                                               outputHeight,
                                               8,
                                               24,
                                               picture.linesize[0],
                                               colorSpace,
                                               bitmapInfo,
                                               provider,
                                               NULL,
                                               NO,
                                               kCGRenderingIntentDefault);
            CGColorSpaceRelease(colorSpace);
            UIImage *last = self.frame;
            UIImage *newframe = [UIImage imageWithCGImage:cgImage];
            self.frame = newframe;
            last = nil;
            newframe = nil;
            CGImageRelease(cgImage);
            CGDataProviderRelease(provider);
            CFRelease(data);
            
            frameCount++;
            
            [NSThread sleepForTimeInterval:interval];
        }
    }
    NSLog(@"%i doDecode finish", self.index);
    [self clearQueue];
    NSLog(@"%i queue clear", self.index);
    [decodeThreadLock unlockWithCondition:DONE];
}
-(void)doRelease{
    @synchronized(self) {
        NSLog(@"CH %i Releasing:", self.index);
        NSLog(@"CH %i is_avformat_open_input: %@", self.index, (is_avformat_open_input)?@"YES":@"NO");
        NSLog(@"CH %i is_avformat_find_stream_info: %@", self.index, (is_avformat_find_stream_info)?@"YES":@"NO");
        NSLog(@"CH %i is_stream_id_found: %@", self.index, (is_stream_id_found)?@"YES":@"NO");
        NSLog(@"CH %i is_avcodec_found: %@", self.index, (is_avcodec_found)?@"YES":@"NO");
        
        if(is_avcodec_video_open && pVideoCodecCtx != NULL) {
            avcodec_close(pVideoCodecCtx);
            is_avcodec_video_open = NO;
        }
        
        if(is_avcodec_audio_open && pAudioCodecCtx != NULL) {
            avcodec_close(pAudioCodecCtx);
            is_avcodec_audio_open = NO;
        }
        
        if(is_avformat_open_input && pFormatCtx!=NULL) {
            avformat_close_input(&pFormatCtx);
            is_avformat_open_input = NO;
        }
        
        if(pFrame != NULL){
            av_frame_free(&pFrame);
        }
        
        [videoQueue clear];
        [audioQueue clear];
    }
}

//
// utilities
//
-(void)setupAudioDecoder {
    /*
     if (audioStream >= 0) {
     _audioBufferSize = AVCODEC_MAX_AUDIO_FRAME_SIZE;
     _audioBuffer = av_malloc(_audioBufferSize);
     _inBuffer = NO;
     
     //_audioCodecContext = pFormatCtx->streams[audioStream]->codec;
     //_audioStream = pFormatCtx->streams[audioStream];
     
     //AVCodec *codec = avcodec_find_decoder(_audioCodecContext->codec_id);
     //if (codec == NULL) {
     //NSLog(@"Not found audio codec.");
     //return;
     //}
     
     //if (avcodec_open2(_audioCodecContext, codec, NULL) < 0) {
     //NSLog(@"Could not open audio codec.");
     //return;
     //}
     
     //if (audioPacketQueue) {
     //[audioPacketQueue release];
     //audioPacketQueue = nil;
     //}
     //audioPacketQueue = [[NSMutableArray alloc] init];
     
     //if (audioPacketQueueLock) {
     //[audioPacketQueueLock release];
     //audioPacketQueueLock = nil;
     //}
     //audioPacketQueueLock = [[NSLock alloc] init];
     
     } else {
     pFormatCtx->streams[audioStream]->discard = AVDISCARD_ALL;
     audioStream = -1;
     }*/
}
-(void)setupScaler {
    // Release old picture and scaler
    avpicture_free(&picture);
    sws_freeContext(pSwsCtx);
    
    // Allocate RGB picture
    avpicture_alloc(&picture, PIX_FMT_RGB24, outputWidth, outputHeight);
    
    // Setup scaler
    static int sws_flags =  SWS_FAST_BILINEAR;
    pSwsCtx = sws_getContext(pVideoCodecCtx->width,
                             pVideoCodecCtx->height,
                             pVideoCodecCtx->pix_fmt,
                             outputWidth,
                             outputHeight,
                             PIX_FMT_RGB24,
                             sws_flags, NULL, NULL, NULL);
    
}
-(int)decodeInterruptCB{
    return isRunningStreamThread;
}
-(void)clearQueue{
    while([videoQueue count] > 0){
        NSData * pktData = [videoQueue dequeue];
        AVPacket pkt;
        pkt.data = (void*)pktData.bytes;
        pkt.size = (int)pktData.length;
        pktData = nil;
        av_init_packet(&pkt);
        av_free_packet(&pkt);
    }
    while([audioQueue count] > 0){
        NSData * pktData = [audioQueue dequeue];
        AVPacket pkt;
        pkt.data = (void*)pktData.bytes;
        pkt.size = (int)pktData.length;
        pktData = nil;
        av_init_packet(&pkt);
        av_free_packet(&pkt);
    }
}
@end
