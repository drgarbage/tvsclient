#ifndef _SEIENCDEC_H_
#define _SEIENCDEC_H_

#import <Foundation/Foundation.h>
//#include <stdlib.h>
//#include <time.h>
//#include <string.h>

#ifndef WIN32
typedef time_t __time32_t;
#endif

typedef struct
{
	__time32_t	tsec;
	float gps0;
	float gps1;
	float dir;
	float speed;
	float gsenx;
	float gseny;
	float gsenz;
	float thermal;
}SUB_DEV_T;

//unsigned char* seiEnc(unsigned char *p, SUB_DEV_T *in, int *seiEncLen);

int seiDec(unsigned char *seiBuf, int *seiBufLen ,SUB_DEV_T *in);



#define EMPTY_VALUE	0xffffffff
/*
template <class T> int seiCheckEmpty(T &p)
{
	unsigned int *iPtr = (unsigned int *)&p;
	if(*iPtr == EMPTY_VALUE)
		return 1;
	return 0;
}
*/
void seiAllFieldEmpty(SUB_DEV_T *p);


/* test app
int main(int argc, _TCHAR* argv[])
{
	SUB_DEV_T in;
	memset(&in, 0xFF, sizeof(in));
	in.tsec = 1234567890;
	in.gps0 = 12.23454;
	in.gps1 = 123.3456;
	in.dir = 0.01;
	in.speed = 100;
	in.thermal = 32.5;
	int seiEncLen;

	unsigned char *p =  seiEnc(&in, &seiEncLen);
	
	SUB_DEV_T out;
	seiDec(p, seiEncLen ,&out);
	printf("%f ", out.gsen);

	if(!checkEmpty(out.gsen))
	{
		printf("%f ", out.gsen);
	}

	printf("\n");
	return 0;
}
*/
#endif
