//
//  AppModel.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "AppModel.h"
#import "VideoDevice.h"
#import "VideoDeviceV2.h"
#import "XMLDictionary.h"

@implementation AppModel
+ (AppModel*) sharedModel{
    @synchronized([AppModel class]){
        if(!INSTANCE){
            NSString *path = [AppModel DocumentPath:@"devices.archive"];
            INSTANCE = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
            
            if (!INSTANCE) {
                INSTANCE = [[AppModel alloc]init];
            }
        }
        return INSTANCE;
    }
}
+ (Device*) newDevice{
    //return [VideoDevice new];
    return [VideoDeviceV2 new];
}
+ (Device*) newDeviceWithURL: (NSString*) url user: (NSString*) user password: (NSString*) password{
    //Device * output = [[VideoDevice alloc] initWithURL:url user:user password:password];
    Device * output = [[VideoDeviceV2 alloc] initWithURL:url user:user password:password];
    return output;
}
+ (void) downloadFromURL: (NSString*) urlString
                    user: (NSString*) user
                password: (NSString*) pwd
                 success: (void(^)(NSString *responseText)) success
                    fail: (void(^)(NSError *error)) fail {
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSString *basicAuthCredentials = [NSString stringWithFormat:@"%@:%@", user, pwd];
    NSData *authData = [basicAuthCredentials dataUsingEncoding:NSASCIIStringEncoding];
    NSString *base64EncodedAuth = [authData base64Encoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@", base64EncodedAuth];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setValue:@"close" forHTTPHeaderField:@"Connection"];
    
    if (fail == nil) {
        fail = ^(NSError *err){
            NSLog(@"Error: %@", err);
        };
    }
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError){
           if (connectionError != nil) {
               fail(connectionError);
               return;
           }
           if(((NSHTTPURLResponse*)response).statusCode == 200) {
               NSString *responseText =
                    [[NSString alloc] initWithData:data
                                          encoding:NSASCIIStringEncoding];
               success(responseText);
           } else {
               NSDictionary *userInfo =
                  @{NSLocalizedDescriptionKey:@"Connection fail, please check account and password."};
               fail([NSError errorWithDomain:urlString
                                        code:((NSHTTPURLResponse*)response).statusCode
                                    userInfo:userInfo]);
           }
       }];
}
+ (void) downloadInfoFromDevice: (Device*) device
                        success: (void(^)(NSDictionary* result)) success
                           fail: (void(^)(NSError *error)) fail{
    NSString *queryString = @"function=2&xml=<REQ>00070100</REQ>";
    NSString *urlString = [NSString stringWithFormat:@"http://%@/cgi-bin/cgistream?%@", device.URL, [queryString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    
    [AppModel downloadFromURL:urlString user:device.UserName password:device.Password success:^(NSString* responseText){
        success([NSDictionary dictionaryWithXMLString:responseText]);
    } fail:fail];
}
+ (void) downloadLogFromDevice: (Device*) device
                       success: (void(^)(NSDictionary* result)) success
                          fail: (void(^)(NSError *error)) fail{
    
    NSString *queryString = @"function=2&xml=<REQ>00071100<STARLOG>0</STARLOG></REQ>";
    NSString *urlString = [NSString stringWithFormat:@"http://%@/cgi-bin/cgistream?%@", device.URL, [queryString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    
    [AppModel downloadFromURL:urlString user:device.UserName password:device.Password success:^(NSString* responseText){
        success([NSDictionary dictionaryWithXMLString:responseText]);
    } fail:fail];
}
- (instancetype) init {
    self = [super init];
    if(self){
        _StoredDevices = [NSMutableArray array];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        _StoredDevices = [aDecoder decodeObjectForKey:@"StoredDevices"];
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_StoredDevices forKey:@"StoredDevices"];
}
- (void) save {
    BOOL result = [NSKeyedArchiver archiveRootObject:self toFile:[AppModel DocumentPath:@"devices.archive"]];
    
    Device *device = [[Device alloc] init];
    device.Name = @"TestDevice";
    device.URL = @"192.168.2.200";
    
    NSString *path = [AppModel DocumentPath:@"test.archive"];
    [NSKeyedArchiver archiveRootObject:device toFile:path];
    id nDevice = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    NSLog(@"device: %@ %@", device.Name, device.URL);
    NSLog(@"nDevice: %@ %@", ((Device*)nDevice).Name, ((Device*)nDevice).URL);
    
//    NSString *error = nil;
//    NSData *mySerializedObject = [NSKeyedArchiver archivedDataWithRootObject:self];
//    NSData *xmlData = [NSPropertyListSerialization dataFromPropertyList:mySerializedObject
//                                                                 format:NSPropertyListXMLFormat_v1_0
//                                                       errorDescription:&error];
//    
//    BOOL result = (xmlData!=nil);
//    
//    if( result ) {
//        [xmlData writeToFile:[AppModel DocumentPath:@"devices.plist"] atomically:YES];
//    }
    
    NSLog(@"Archive: %@", (result)?@"Success":@"Failure");
}

+ (NSString*) DocumentPath: (NSString*) filename{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:filename];
    return path;
}
@end