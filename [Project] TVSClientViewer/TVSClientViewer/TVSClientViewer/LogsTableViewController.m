//
//  LogsTableViewController.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/30.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "LogsTableViewController.h"
#import "AppModel.h"
#import "XMLDictionary.h"

@interface LogsTableViewController () {
    NSMutableArray *logs;
}

@end

@implementation LogsTableViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    logs = [NSMutableArray array];
    
    Device *device = [AppModel sharedModel].CurrentDevice;
    [AppModel downloadLogFromDevice:device success:^(NSDictionary *dic) {
        [logs removeAllObjects];
        for(id logItem in dic[@"ITEM"])
            [logs addObject:logItem[@"LOG"]];
        [self.tableView performSelectorOnMainThread : @selector(reloadData) withObject:nil waitUntilDone:YES];
    } fail:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return logs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier: @"LogItem"
                                        forIndexPath:indexPath];
    
    cell.textLabel.text = [NSString stringWithFormat: @"%li", (long)indexPath.row + 1];
    cell.detailTextLabel.text = logs[indexPath.row];
    
    return cell;
}

@end
