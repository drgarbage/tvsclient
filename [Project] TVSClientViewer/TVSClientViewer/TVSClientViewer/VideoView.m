//
//  VideoView.m
//  TVSClientViewer
//
//  Created by Johnson on 2015/9/29.
//  Copyright © 2015年 tvs. All rights reserved.
//

#import "VideoView.h"

@interface VideoView () {
    NSTimer *timer;
    BOOL isStop;
}
@end

@implementation VideoView

- (void)setChannel:(Channel *)channel{
    if(_channel != channel) {
        [_channel stop];
    }
    _channel = channel;
    if(_channel != nil) {
        [_channel play:[NSDate new]];
    }
    if(timer == nil) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0/30.0
                                                 target:self
                                               selector:@selector(frame:)
                                               userInfo:nil
                                                repeats:YES];
    }
}

- (void)frame: (id) state {
    self.image = (self.channel != nil)?
        self.channel.frame:nil;
}

@end