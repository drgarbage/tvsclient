#include "SeiEncDec.h"

#include <stdio.h>
#include <string.h>
/*
SeiEncDec::SeiEncDec(void)
{
}

SeiEncDec::~SeiEncDec(void)
{
}

*/

 
#include <time.h>

typedef enum
{
	isInt,
	isFloat
}DTYPE_E;

typedef struct
{
	char			name[8];	
	DTYPE_E			dtype;
}CODEBOOK_T;

static CODEBOOK_T codebook[]=
{
	{"tsec",	isInt},	//0
	{"gps0",	isFloat},	//1
	{"gps1",	isFloat},	//2
	{"dir",		isFloat},	//3
	{"speed",	isFloat},	//4
	{"gsenx",	isFloat},	//5
	{"gseny",	isFloat},	//5
	{"gsenz",	isFloat},	//5
	{"thml",	isFloat}	//6
};

unsigned char* seiEnc(unsigned char *p, SUB_DEV_T *in, int *seiEncLen)
{
	p[0] = 0x6;
	p[1] = 0x5;

	unsigned int *pField[] = 
	{
		(unsigned int *)&in->tsec,
		(unsigned int *)&in->gps0,
		(unsigned int *)&in->gps1,
		(unsigned int *)&in->dir,
		(unsigned int *)&in->speed,
		(unsigned int *)&in->gsenx,
		(unsigned int *)&in->gseny,
		(unsigned int *)&in->gsenz,
		(unsigned int *)&in->thermal
	};

	int x = 4;
	int item = 0;
	for(int i = 0; i < sizeof(pField)/sizeof(unsigned int *); i++)
	{
		if( codebook[i].dtype == isInt &&  *pField[i] != EMPTY_VALUE )
		{
			p[x] = i;
			memcpy(&p[x+1], pField[i], sizeof(int));
			x += sizeof(int)+1;
			item ++;
		}
		
		if( codebook[i].dtype == isFloat && *pField[i] != EMPTY_VALUE )
		{
			p[x] = i;
			memcpy(&p[x+1], pField[i], sizeof(float));
			x += sizeof(float)+1;
			item ++;
		}
	}

	p[2] = (x)-3;
	p[3] = item;
	p[x] = 0x80;
	p[x+1] = '\0';

	if(x == 0)
	{
		delete [] p;
		p = NULL;
	}

	*seiEncLen = x+1;
	return p;
}



int seiDec(unsigned char *seiBuf, int *seiBufLen ,SUB_DEV_T *out)
{
	int items = seiBuf[3];
	memset(out, 0xff, sizeof(out));
	
	int tagPos = seiBuf[2] + 3;
	if( !(seiBuf[0] == 6 && seiBuf[1] == 5 && seiBuf[tagPos] == 0x80) )
		return 0;
	

	unsigned int *pField[] = 
	{
		(unsigned int *)&out->tsec,
		(unsigned int *)&out->gps0,
		(unsigned int *)&out->gps1,
		(unsigned int *)&out->dir,
		(unsigned int *)&out->speed,
		(unsigned int *)&out->gsenx,
		(unsigned int *)&out->gseny,
		(unsigned int *)&out->gsenz,
		(unsigned int *)&out->thermal
	};

	int x = 4;
	for(int i = 0; i < items; i++)
	{
		int t = seiBuf[x];
		if( codebook[t].dtype == isInt )
		{
			memcpy(pField[t], &seiBuf[x+1], sizeof(int));
			x += sizeof(int) + 1;
		}
		if( codebook[t].dtype == isFloat )
		{
			memcpy(pField[t], &seiBuf[x+1], sizeof(float));
			x += sizeof(float) + 1;
		}
	}

	return x+1;
}



void seiAllFieldEmpty(SUB_DEV_T *p)
{
	memset(p, 0xff, sizeof(SUB_DEV_T));
}






